<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Foundation\Auth\User as Authenticatable;

class Franquicia extends Model
{
    protected $table = 'franquicia';
    protected $guarded = ['id'];
    
    function __construct(array $attributes = array()) {
        parent::__construct($attributes);}
    // protected $guarded = ['password'];
}
