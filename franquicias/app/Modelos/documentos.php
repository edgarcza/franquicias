<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Foundation\Auth\User as Authenticatable;

class documentos extends Model
{
    protected $table = 'documentos';
    protected $guarded = ['id'];
    
    function __construct(array $attributes = array()) {
        parent::__construct($attributes);}
}
