<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    protected $table = 'usuarios';
    protected $fillable = ['nombres', 'apellidos', 'usuario', 'password'];
    // protected $guarded = ['password'];
    function __construct(array $attributes = array()) {
        parent::__construct($attributes);}
}
