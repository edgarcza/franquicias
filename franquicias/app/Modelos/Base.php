<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{    
    protected $guarded = ['id'];
    
    public $Datos;
    public $Joins;

    public $F_Datos;
    public $F_Datos_Total;

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    public function Guardar($datos) {
        $Guardar = $datos;
        $Guardado = null;

        if(isset($Guardar['id'])) {
            $Guardado = $this::find($Guardar['id']);
            $Guardado->fill($Guardar);
            $Guardado->save();
            $this->Datos = $Guardado;
        }
        else {
            $Guardado = $this::create($Guardar);
            $this->Datos = $Guardado;
        }

        if(!empty($Guardado))
            return true;
        return false;
    }

    public function Eliminar($id) {
        $ID = $id;

        $Eliminado = $this::where('id', $ID)->delete();
        // $Usuario;

        if(!empty($Eliminado))
            return true;
        return false;
    }
    
    public function Filtrar($filtros, $paginador = null, $select = null, $joins = null) {
        // $Filtros = $datos['Filtros'];
        // $Paginador = (isset($datos['Paginador'])) ?
        //     $datos['Paginador'] : null;
        $Filtros = $filtros;
        $Paginador = $paginador;
        
        $Query = $this::query();

        $JoinsR = (isset($joins)) ? $joins : $this->Joins;

        if(!empty($JoinsR)) {
            foreach ($JoinsR as $Join) {
                $Query->join($Join[0], $Join[1], $Join[2]);
            }
        }

        if(!empty($this->Orders)) {
            foreach ($this->Orders as $Order) {
                $Query->orderBy($Order[0], $Order[1]);
            }
        }

        foreach ($Filtros as $Filtro) {
            if(isset($Filtro['Condicion']) && !empty($Filtro['Condicion']))
                $Query = $Query->where($Filtro['Campo'], $Filtro['Condicion'], $Filtro['Valor']);
            else
                $Query = $Query->where($Filtro['Campo'], 'like', '%'.$Filtro['Valor'].'%');
        }

        if(!isset($select))
            $Query = $Query->select('*', (new $this)->getTable().'.id as id_principal');
        else
        $Query = $Query->select($select);

        $DatosUL = $Query->get()->count();

        if(!empty($Paginador)) {
            $Query = $Query->offset($Paginador['Cantidad'] * $Paginador['Pagina']);
            $Query = $Query->limit($Paginador['Cantidad']);
        }
        $Datos = $Query->get();

        $this->F_Datos = $Datos;
        $this->F_Datos_Total = $DatosUL;

        return $Datos;

        // return response()->json(['proceso' => true, 'datos' => $Datos, 'total' => $DatosUL, 'req' => $request->all()['datos'], 'joins' => $this->Joins]);
    }
    
}