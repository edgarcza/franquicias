<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class UsuarioArchivo extends Model
{
    protected $table = 'lpf';
    protected $guarded = ['id'];
    
    function __construct(array $attributes = array()) {
        parent::__construct($attributes);}
}
