<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Orden extends Base
{
    protected $table = 'ordenes';
    protected $guarded = ['id'];

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->Joins = [
            ["cursos", "ordenes.id_curso", "cursos.id"],
            ["alumnos", "ordenes.id_alumno", "alumnos.id"],
            ["materias", "cursos.id_materia", "materias.id"],
            ["instituciones", "ordenes.id_institucion", "instituciones.id"],
            ["tipos_curso", "cursos.id_tipo_curso", "tipos_curso.id"],
            ["ordenes_estatus", "ordenes_estatus.id", "ordenes.id_estatus"],
            ["tipos_credito", "cursos.id_tipo_credito", "tipos_credito.id"],
            ["formatos", "ordenes.id_formato", "formatos.id"],
            ["sexos", "alumnos.id_sexo", "sexos.id"]
        ];
    }
    
}
