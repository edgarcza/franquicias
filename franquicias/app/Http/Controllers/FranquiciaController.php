<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Modelos\Franquicia;
use App\Modelos\Franquicia as Modelo;

class FranquiciaController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Franquicia';
    }
}
