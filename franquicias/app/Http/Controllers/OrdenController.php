<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Modelos\Orden as Modelo;
use App\Modelos\Alumno;
use App\Modelos\PagoComisionD;
use App\Modelos\PagoComision;
use App\Modelos\Pago;
use App\Modelos\PagoOrden;
use App\Modelos\PagoPlataforma;
use App\Modelos\InstitucionesAsignaciones as InstitucionAsignacion;

use App\Exports\PEFExport;
use App\Imports\AlumnosImport;
use Maatwebsite\Excel\Facades\Excel;

class OrdenController extends BaseController
{

    function __construct() {
        $this->Modelo = 'App\Modelos\Orden';
        // $this->Joins = [
        //     ["cursos", "ordenes.id_curso", "cursos.id"],
        //     ["alumnos", "ordenes.id_alumno", "alumnos.id"],
        //     ["materias", "cursos.id_materia", "materias.id"],
        //     ["instituciones", "ordenes.id_institucion", "instituciones.id"],
        //     ["tipos_curso", "cursos.id_tipo_curso", "tipos_curso.id"],
        //     ["ordenes_estatus", "ordenes_estatus.id", "ordenes.id_estatus"],
        //     ["sexos", "alumnos.id_sexo", "sexos.id"]
        // ];
    }

    function GuardarYComision(Request $request) {
        $Guardar = $request->all()['datos'];
        $Guardado = null;
        
        $Modelo = new $this->Modelo();

        // $Guardado = $Modelo->create($Guardar);
        // $Guardado = $this->Modelo::create([
        //     "id_curso" => $Guardar['id_curso'],
        //     "id_alumno" => $Guardar['id_alumno'],
        // ]);
        // return response()->json(['req' => $Guardar, 'req2' => $Guardado]);

        if($Modelo->Guardar($Guardar)) {

            if(isset($Guardar['id']) && !empty($Guardar['id'])) {
                $PagosDAnteriores = PagoComisionD::where('id_orden', $Modelo->Datos['id'])->get();
                foreach ($PagosDAnteriores as $Pago) {
                    $PagoEliminado = PagoComision::where('id', $Pago['id_pago_comision'])->delete();
                    $PagoDEliminado = PagoComisionD::where('id', $Pago['id'])->delete();
                }
            }

            $Personas = InstitucionAsignacion::where('id_institucion', '=', $Modelo->Datos['id_institucion'])->get();
            foreach ($Personas as $Persona) {
                $Pago = array();
                $Pago['monto_usd'] = round($Persona['iacomision'] / 100 * $Modelo->Datos['precio_publico_usd'], 2);
                $Pago['monto_mxn'] = round($Persona['iacomision'] / 100 * $Modelo->Datos['precio_publico_mxn'], 2);
                $Pago['id_orden'] = $Modelo->Datos['id'];
                $Pago['id_persona'] = $Persona['id_persona'];
                $PagoGuardado = PagoComisionD::create($Pago);
            }

            return response()->json(['proceso' => true, 'datos' => $Modelo->Datos, 'req' => $Guardar]);
        }

        // if(isset($Guardar['id'])) {
        //     $Guardado = $this->Modelo::find($Guardar['id']);
        //     $Guardado->fill($Guardar);
        //     $Guardado->save();

        //     $PagosDAnteriores = PagoComisionD::where('id_orden', $Guardado['id'])->get();
        //     foreach ($PagosDAnteriores as $Pago) {
        //         $PagoEliminado = PagoComision::where('id', $Pago['id_pago_comision'])->delete();
        //         $PagoDEliminado = PagoComisionD::where('id', $Pago['id'])->delete();
        //     }

        //     $Personas = InstitucionAsignacion::where('id_institucion', '=', $Guardado['id_institucion'])->get();
        //     foreach ($Personas as $Persona) {
        //         $Pago['monto_usd'] = round($Persona['iacomision'] / 100 * $Guardado['precio_publico_usd'], 2);
        //         $Pago['monto_mxn'] = round($Persona['iacomision'] / 100 * $Guardado['precio_publico_mxn'], 2);
        //         $Pago['id_orden'] = $Guardado['id'];
        //         $Pago['id_persona'] = $Persona['id_persona'];
        //         $PagoGuardado = PagoComisionD::create($Pago);
        //     }
        // }
        // else {
        //     $Guardado = $this->Modelo::create($Guardar);

        //     $Personas = InstitucionAsignacion::where('id_institucion', '=', $Guardado['id_institucion'])->get();
        //     foreach ($Personas as $Persona) {
        //         $Pago['monto_usd'] = round($Persona['iacomision'] / 100 * $Guardado['precio_publico_usd'], 2);
        //         $Pago['monto_mxn'] = round($Persona['iacomision'] / 100 * $Guardado['precio_publico_mxn'], 2);
        //         $Pago['id_orden'] = $Guardado['id'];
        //         $Pago['id_persona'] = $Persona['id_persona'];
        //         $PagoGuardado = PagoComisionD::create($Pago);
        //     }

        // }

        // if(!empty($Guardado))
        //     return response()->json(['proceso' => true, 'datos' => $Guardado, 'req' => $Guardar]);
        return response()->json(['proceso' => false, 'datos' => $Guardar]);
    }

    function Materias(Request $request) {
        $Tipo = $request->all()['datos']['tipo'];
        $ID = $request->all()['datos']['id'];

        $Modelo = new $this->Modelo();

        $Query = $this->Modelo::query();

        if(!empty($Modelo->Joins)) {
            foreach ($Modelo->Joins as $Join) {
                $Query->join($Join[0], $Join[1], $Join[2]);
            }
        }

        if($Tipo == 1)
            $Query = $Query->where('id_institucion', '=', $ID);
        else if($Tipo == 2)
            $Query = $Query->where('id_alumno', '=', $ID);

        $Query = $Query->groupBy('id_materia');

        $Datos = $Query->get();
        // $Query = $Query->select('*', ''.id as id_principal');
        // Datos
        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }

    function Cuentas(Request $request) {
        $Filtros = $request->all()['datos']['Filtros'];

        $Query = Pago::query();
        $Datos['ingresos']['pagos_institucion'] = $Query
                ->where("pagos.tipo", "=", 1)
                ->where("pagos.fecha_pago", ">=", $Filtros['desde'])
                ->where("pagos.fecha_pago", "<=", $Filtros['hasta'])
                ->groupBy("pagos.tipo")
                ->first(['pagos.tipo', DB::raw('SUM(monto_usd) AS usd'), DB::raw('SUM(monto_mxn) AS mxn')]);
        $Datos['ingresos']['pagos_institucion']['usd'] = (isset($Datos['ingresos']['pagos_institucion']['usd'])) ? $Datos['ingresos']['pagos_institucion']['usd'] : 0;
        $Datos['ingresos']['pagos_institucion']['mxn'] = (isset($Datos['ingresos']['pagos_institucion']['mxn'])) ? $Datos['ingresos']['pagos_institucion']['mxn'] : 0;

        $Query = PagoPlataforma::query();
        $Datos['ingresos']['pagos_plataforma'] = $Query
                ->where("pagos_plataforma.id_estado", "=", 2)
                ->where("pagos_plataforma.fecha_pago", ">=", $Filtros['desde'])
                ->where("pagos_plataforma.fecha_pago", "<=", $Filtros['hasta'])
                ->groupBy("pagos_plataforma.id_estado")
                ->first(['pagos_plataforma.id_estado', DB::raw('SUM(pago) AS usd')]);
        $Datos['ingresos']['pagos_plataforma']['usd'] = (isset($Datos['ingresos']['pagos_plataforma']['usd'])) ? $Datos['ingresos']['pagos_plataforma']['usd'] : 0;
        $Datos['ingresos']['pagos_plataforma']['mxn'] = (isset($Datos['ingresos']['pagos_plataforma']['mxn'])) ? $Datos['ingresos']['pagos_plataforma']['mxn'] : 0;

        $Datos['ingresos']['total_usd'] = 
            $Datos['ingresos']['pagos_plataforma']['usd'] + 
            $Datos['ingresos']['pagos_institucion']['usd'];
        $Datos['ingresos']['total_mxn'] = 
            $Datos['ingresos']['pagos_institucion']['mxn'];

        $Query = Pago::query();
        $Datos['egresos']['pagos_keystone'] = $Query
                ->where("pagos.tipo", "=", 2)
                ->where("pagos.fecha_pago", ">=", $Filtros['desde'])
                ->where("pagos.fecha_pago", "<=", $Filtros['hasta'])
                ->groupBy("pagos.tipo")
                ->first(['pagos.tipo', DB::raw('SUM(monto_usd) AS usd'), DB::raw('SUM(monto_mxn) AS mxn')]);
        $Datos['egresos']['pagos_keystone']['usd'] = (isset($Datos['egresos']['pagos_keystone']['usd'])) ? $Datos['egresos']['pagos_keystone']['usd'] : 0;
        $Datos['egresos']['pagos_keystone']['mxn'] = (isset($Datos['egresos']['pagos_keystone']['mxn'])) ? $Datos['egresos']['pagos_keystone']['mxn'] : 0;

        $Query = PagoComision::query();
        $Datos['egresos']['comisiones'] = $Query
                ->where("pagos_comisiones.estatus", ">", 0)
                ->where("pagos_comisiones.fecha_pago", ">=", $Filtros['desde'])
                ->where("pagos_comisiones.fecha_pago", "<=", $Filtros['hasta'])
                ->groupBy("pagos_comisiones.estatus")
                ->first(['pagos_comisiones.estatus', DB::raw('SUM(monto_usd) AS usd'), DB::raw('SUM(monto_mxn) AS mxn')]);
        $Datos['egresos']['comisiones']['usd'] = (isset($Datos['egresos']['comisiones']['usd'])) ? $Datos['egresos']['comisiones']['usd'] : 0;
        $Datos['egresos']['comisiones']['mxn'] = (isset($Datos['egresos']['comisiones']['mxn'])) ? $Datos['egresos']['comisiones']['mxn'] : 0;

        $Datos['egresos']['total_usd'] = 
            $Datos['egresos']['pagos_keystone']['usd'] + 
            $Datos['egresos']['comisiones']['usd'];
        $Datos['egresos']['total_mxn'] = 
            $Datos['egresos']['pagos_keystone']['mxn'] +
            $Datos['egresos']['comisiones']['mxn'];

        $Datos['margen']['usd'] = $Datos['ingresos']['total_usd'] - $Datos['egresos']['total_usd'];
        $Datos['margen']['mxn'] = $Datos['ingresos']['total_mxn'] - $Datos['egresos']['total_mxn'];
        
        return response()->json(['proceso' => true, 'datos' => $Datos, 'filtros' => $Filtros]);
    }

    
    function Cancelacion(Request $request) {
        $ID = $request->all()['datos']['id_orden'];

        $Orden = Modelo::find($ID);

        $Datos['orden'] = $Orden;
        $Datos['dias'] = abs(strtotime(date('Y-m-d')) - strtotime($Orden['fecha_inscripcion'])) / (60 * 60 * 24);

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }
    
    function Cancelar(Request $request) {
        $ID = $request->all()['datos']['id_orden'];
        $Reembolso = $request->all()['datos']['reembolso'];

        $Orden = Modelo::find($ID);

        $Estado = 4;
        if($Reembolso == 2) 
            $Estado = 3; // cancelar solo

        $Datos['orden'] = $Orden;
        $Datos['dias'] = abs(strtotime(date('Y-m-d')) - strtotime($Orden['fecha_inscripcion'])) / (60 * 60 * 24);

        $NOrden = $this->Modelo::find($ID);
        $NOrden->fill(['id_estatus' => $Estado]);
        $NOrden->save();

        $Alumno = Alumno::where("alumnos.id", "=", $NOrden['id_alumno'])->first();
        $Alumno->cancelaciones = $Alumno->cancelaciones + 1;
        $Alumno->save();

        $Datos['alumno'] = $Alumno;


        if($Datos['dias'] <= 30 && $Reembolso != 2) {
            // Con reembolso
            // Borrar de pagos_ordenes y pagos_comisiones_d los de id_orden = $ID
            $Datos['comisiones'] = PagoComisionD::where("pagos_comisiones_d.id_orden", "=", $ID)->delete();
            $Datos['pagos_ordenes'] = PagoOrden::where("pagos_ordenes.id_orden", "=", $ID)->delete();
        }

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }

    public function PEF(Request $request) {
        $RD = $request->all()['datos'];
        $Modelo = new $this->Modelo;
        $Datos = $Modelo->Filtrar($RD['Filtros'], $RD['Paginador']);

        $DxA = $this->Modelo::query()
            ->select(DB::raw('ordenes.id_alumno, COUNT(*) as cantidad'))
            ->groupBy('ordenes.id_alumno')
            ->get();

        foreach ($Datos as $i => $Orden) {
            $Orden['cantidad_ordenes'] = 0;
            foreach ($DxA as $AlumnoCantidad) {
                if($Orden['id_alumno'] == $AlumnoCantidad['id_alumno'])
                    $Orden['cantidad_ordenes'] = $AlumnoCantidad['cantidad'];
            }
        }

        // $var = 0;
        // foreach ($Datos as $i => $valor) {
        //     $var ++;
        // }


        $AN = 'Partner Enrollment Form ' . date("d-m-Y H:i:s") . '.xlsx';
        Excel::store(new PEFExport($Datos), 'pef/'.$AN);
        

        return response()->json(['proceso' => false, 
            'datos' => $Datos, 'excel' => $AN,
            'dxa' => $DxA,
            // 'var' => $var,
            ]);
    }

    public function Importar(Request $request) {
        $Orden = $request->all()['datos']['Orden'];
        $PK = $request->all()['datos']['PK'];
        $PI = $request->all()['datos']['PI'];
        $TC = $request->all()['datos']['TipoCambio'];

        $Base64_Entero = $Orden['excel'];
        $Base64 = explode(',', $Base64_Entero)[1];

        if(file_put_contents('importar/estudiantes/' . $Orden['excel_nombre'], base64_decode($Base64))) {
            $OrdenBase = array(
                'id_curso' => $Orden['id_curso'],
                'id_formato' => $Orden['id_formato'],
                // 'id_grado' => $Orden['id_grado'],
                'id_institucion' => $Orden['id_institucion'],
                // 'id_tipo_credito' => $Orden['id_tipo_credito'],
                // 'id_tipo_curso' => $Orden['id_tipo_curso'],
                // 'id_transcript' => $Orden['id_transcript'],
                'materiales' => $Orden['materiales'],
                'id_tipo_cambio' => $TC['id'],
                'id_estatus' => 1,
                'fecha_inscripcion' => date("Y-m-d"),

                'precio_keystone' => $PK['precio_keystone'],
                'descuento' => $PK['descuento'],
                'envio' => $PK['envio'],
                'cargo_transferencia' => $PK['cargo_transferencia'],
                'extensiones' => $PK['extensiones'],
                'total_keystone' => $PK['total'],

                'adicionales_mxn' => $PI['adicionales_mxn'],
                'adicionales_usd' => $PI['adicionales_usd'],
                'precio_publico_mxn' => $PI['monto_mxn'],
                'precio_publico_usd' => $PI['monto_usd'],
                'iva' => $PI['iva'],
                'total_publico_usd' => $PI['total_usd'],
                'total_publico_mxn' => $PI['total_mxn'],
            );

            Excel::import(new AlumnosImport($OrdenBase), 'importar/estudiantes/' . $Orden['excel_nombre']);

            return response()->json(['proceso' => true, 'datos' => $Orden]);
        }

    }

    public function PruebaPEF() {
        $datos = [['nombres' => 'asd']];
        return view('excel.pef', compact('datos'));
    }
    
}
