<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Modelos\Institucion;
use App\Modelos\Institucion as Modelo;
use App\Modelos\InstitucionDocumento;
use App\Modelos\Upload;

class InstitucionController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Institucion';
    }
    function Documentos(Request $request) {
        $Upload = new Upload($request->all()['datos']);
        $ArchivoGuardado = InstitucionDocumento::create([
            'comentario' => $Upload->Descripcion, 
            'extension' => $Upload->Extension, 
            'id_institucion' => $Upload->Datos['id_institucion']
        ]);

        $Upload -> $Datos['id']
        
        if($Upload->Guardar($ArchivoGuardado['id'])) {
            return response()->json([
                'proceso' => true, 
                // 'datos' => $Archivo, 
                // '64' => $Archivo64, 
                'Info' => $ArchivoGuardado
            ]);
        }
        return response()->json(['proceso' => false, 
            'datos' => $Datos, 
            // '64' => $Archivo64, 
            // 'Info' => $ArchivoGuardado
        ]);
    }
}
