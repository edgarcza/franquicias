<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    public $Modelo;
    // public $Joins;
    
    public function Guardar(Request $request) {
        $Guardar = $request->all()['datos'];
        $Modelo = new $this->Modelo;
        if($Modelo->Guardar($Guardar))
            return response()->json(['proceso' => true, 'datos' => $Modelo->Datos]);
        return response()->json(['proceso' => false, 'datos' => $Guardar]);

        // $Guardar = $request->all()['datos'];
        // $Guardado = null;

        // if(isset($Guardar['id'])) {
        //     $Guardado = $this->Modelo::find($Guardar['id']);
        //     $Guardado->fill($Guardar);
        //     $Guardado->save();
        // }
        // else {
        //     $Guardado = $this->Modelo::create($Guardar);
        // }

        // if(!empty($Guardado))
        //     return response()->json(['proceso' => true, 'datos' => $Guardado]);
        // return response()->json(['proceso' => false, 'datos' => $Guardar]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->all()['datos'];
        $Modelo = new $this->Modelo;
        if($Modelo->Eliminar($ID))
            return response()->json(['proceso' => true, 'datos' => $Modelo->Datos]);
        return response()->json(['proceso' => false, 'datos' => $ID]);


        // $Eliminado = $this->Modelo::where('id', $ID)->delete();
        // // $Usuario;

        // if(!empty($Eliminado))
        //     return response()->json(['proceso' => true, 'datos' => $Eliminado]);
        // return response()->json(['proceso' => false, 'datos' => $ID]);
    }
    
    public function Filtrar(Request $request) {
        $Datos = $request->all()['datos'];
        $Modelo = new $this->Modelo;
        // $Filas = $Modelo->Filtrar($Datos);
        $Filas = $Modelo->Filtrar($Datos['Filtros'],
            isset($Datos['Paginador']) ? $Datos['Paginador'] : null);
        return response()->json(['proceso' => true, 'datos' => $Filas, 'total' => $Modelo->F_Datos_Total, 'req' => $Datos, 'joins' => $Modelo->Joins]);
        // if($Modelo->Eliminar($ID))
        //     return response()->json(['proceso' => true, 'datos' => $Modelo->Datos]);
        // return response()->json(['proceso' => false, 'datos' => $ID]);

        // $Filtros = $request->all()['datos']['Filtros'];
        // $Paginador = (isset($request->all()['datos']['Paginador'])) ?
        //     $request->all()['datos']['Paginador'] : null;
        
        // $Query = $this->Modelo::query();

        // if(!empty($this->Joins)) {
        //     foreach ($this->Joins as $Join) {
        //         $Query->join($Join[0], $Join[1], $Join[2]);
        //     }
        // }

        // if(!empty($this->Orders)) {
        //     foreach ($this->Orders as $Order) {
        //         $Query->orderBy($Order[0], $Order[1]);
        //     }
        // }

        // foreach ($Filtros as $Filtro) {
        //     if(isset($Filtro['Condicion']) && !empty($Filtro['Condicion']))
        //         $Query = $Query->where($Filtro['Campo'], $Filtro['Condicion'], $Filtro['Valor']);
        //     else
        //         $Query = $Query->where($Filtro['Campo'], 'like', '%'.$Filtro['Valor'].'%');
        // }

        // $Query = $Query->select('*', (new $this->Modelo)->getTable().'.id as id_principal');

        // $DatosUL = $Query->get()->count();

        // if(!empty($Paginador)) {
        //     $Query = $Query->offset($Paginador['Cantidad'] * $Paginador['Pagina']);
        //     $Query = $Query->limit($Paginador['Cantidad']);
        // }
        // $Datos = $Query->get();

        // return response()->json(['proceso' => true, 'datos' => $Datos, 'total' => $DatosUL, 'req' => $request->all()['datos'], 'joins' => $this->Joins]);
    }
    
    public function Todas(Request $request) {
        // $Filtros = $request->all()['datos']['Filtros'];
        // $Paginador = $request->all()['datos']['Paginador'];
        
        $Datos = $this->Modelo::all();
        // $Datos = $Query->get();

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }
}
?>