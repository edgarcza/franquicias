<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\calendario as Modelo;

class calendarioController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\calendario';
    }
    function Institucion(Request $request) {
        $Institucion = $request->all()['datos']['Franquicia'];
        $Query = Modelo::query();
        $Query = $Query->where('id_franquicia', '=', $Institucion);
        $Query = $Query->groupBy('fecha_inicio');
        $Grupos = $Query->get();

        $Datos = [];

        $iFecha = 0;
        foreach ($Grupos as $Grupo) {
            $QG = Modelo::query();
            $QG = $QG->where('id_franquicia', '=', $Institucion);
            $QG = $QG->where('fecha_inicio', '=', $Grupo['fecha_inicio']);
            $QG = $QG->groupBy('hora_inicio');
            $QG = $QG->groupBy('minuto_inicio');
            $QG = $QG->orderBy('hora_inicio', 'ASC');
            $QG = $QG->orderBy('minuto_inicio', 'ASC');
            $GruposDia = $QG->get();
            $Datos['calendario'][$iFecha]['fecha_inicio'] = $Grupo['fecha_inicio'];
            $Datos['calendario'][$iFecha]['fecha_inicio_ts'] = strtotime($Grupo['fecha_inicio']) * 1000;
            $iHora = 0;
            foreach ($GruposDia as $GrupoDia) {
                $QG = Modelo::query();
                $QG = $QG->where('id_franquicia', '=', $Institucion);
                $QG = $QG->where('fecha_inicio', '=', $GrupoDia['fecha_inicio']);
                $QG = $QG->where('hora_inicio', '=', $GrupoDia['hora_inicio']);
                $QG = $QG->where('minuto_inicio', '=', $GrupoDia['minuto_inicio']);
                $QG = $QG->orderBy('hora_inicio', 'ASC');
                $QG = $QG->orderBy('minuto_inicio', 'ASC');
                $Datos['calendario'][$iFecha]['grupo'][$iHora]['hora'] = $GrupoDia['hora_inicio'] . ':' . $GrupoDia['minuto_inicio'];
                $Datos['calendario'][$iFecha]['grupo'][$iHora]['tareas'] = $QG->get();
                $iHora++;
            }
            $iFecha++;
        }

       /*  $iTarea = 0;
        foreach ($Datos['calendario'] as $Tarea) {
            // $Datos['calendario'][$iTarea]['documentos'] = TareaArchivo::query()->where('id_tarea', $Tarea['id'])->get();
            $iTarea++;
        } */

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }

    function InstitucionDB(Request $request) {
        
        $fecha_actual = date("Y'-'mm '-'dd");
        $Institucion = $request->all()['datos']['Franquicia'];
        $Query = Modelo::query();
        $Query = $Query->where('fecha_inicio', '>',$fecha_actual);
        $Query = $Query->groupBy('fecha_inicio');
        $Grupos = $Query->get();

        $Datos = [];

        $iFecha = 0;
        foreach ($Grupos as $Grupo) {
            $QG = Modelo::query();
            $Query = $Query->where('fecha_inicio', '>',$fecha_actual);
            $QG = $QG->where('fecha_inicio', '=', $Grupo['fecha_inicio']);
            $QG = $QG->groupBy('hora_inicio');
            $QG = $QG->groupBy('minuto_inicio');
            $QG = $QG->orderBy('hora_inicio', 'ASC');
            $QG = $QG->orderBy('minuto_inicio', 'ASC');
            $GruposDia = $QG->get();
            $Datos['calendario'][$iFecha]['fecha_inicio'] = $Grupo['fecha_inicio'];
            $Datos['calendario'][$iFecha]['fecha_inicio_ts'] = strtotime($Grupo['fecha_inicio']) * 1000;
            $iHora = 0;
            foreach ($GruposDia as $GrupoDia) {
                $QG = Modelo::query();
                // $QG = $QG->where('id_franquicia', '=', $Institucion);
                $QG = $QG->where('fecha_inicio', '=', $GrupoDia['fecha_inicio']);
                $QG = $QG->where('hora_inicio', '=', $GrupoDia['hora_inicio']);
                $QG = $QG->where('minuto_inicio', '=', $GrupoDia['minuto_inicio']);
                $QG = $QG->orderBy('hora_inicio', 'ASC');
                $QG = $QG->orderBy('minuto_inicio', 'ASC');
                $Datos['calendario'][$iFecha]['grupo'][$iHora]['hora'] = $GrupoDia['hora_inicio'] . ':' . $GrupoDia['minuto_inicio'];
                $Datos['calendario'][$iFecha]['grupo'][$iHora]['tareas'] = $QG->get();
                $iHora++;
            }
            $iFecha++;
        }

       /*  $iTarea = 0;
        foreach ($Datos['calendario'] as $Tarea) {
            // $Datos['calendario'][$iTarea]['documentos'] = TareaArchivo::query()->where('id_tarea', $Tarea['id'])->get();
            $iTarea++;
        } */

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }
}
