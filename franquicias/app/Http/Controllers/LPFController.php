<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Modelos\LPF;
use App\Modelos\lpf as Modelo;
use App\Modelos\documentos;
use App\Modelos\Franquicia;
use App\Modelos\Usuario;
use App\Modelos\Upload;

class lpfController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\lpf';
    }
    function Imagenes(Request $request)
    {
        $Datos = $request ->all()['datos'];
        $Archivo64 = $request->all()['datos']['Base64'];
        $Archivo = explode(',', $Archivo64)[1];
        $ArchivoTipo = explode('/', finfo_buffer(finfo_open(), base64_decode($Archivo), FILEINFO_MIME_TYPE))[1];
        $ArchivoGuardado = lpf::create(['descripcion' => $Datos['Nombre'], 'extension' => $ArchivoTipo, 'id_tarea' => $Datos['Nombre']]);
        if(!empty($ArchivoGuardado)) {
            $Dir = 'archivos/' . $Datos['Carpeta'];
            if(!is_dir($Dir)) mkdir($Dir);
            $Ruta = $Dir . '/' . $ArchivoGuardado['id'] . '.' . $ArchivoTipo;	
            if(file_put_contents($Ruta, base64_decode($Archivo)))
                return response()->json(['proceso' => true, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
        }
        return response()->json(['proceso' => false, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
    
    }
    function Archivos(Request $request) {
        // $Datos = "";
        // $Datos = $request->all()['datos'];
        // $Archivo64 = $request->all()['datos']['Base64'];
        // $Archivo = explode(',', $Archivo64)[1];
        $Upload = new Upload($request->all()['datos']);

        $LPF = Modelo::find($Upload->Datos['id']);
        $LPF->foto = $Upload->Extension;
        $LPF->save();
        
        if($Upload->Guardar($Upload->Datos['id'])) {
            return response()->json([
                'proceso' => true, 
                // 'datos' => $Archivo, 
                // '64' => $Archivo64, 
                'Info' => $Upload
            ]);
        }
        
        // $ArchivoTipo = explode('/', finfo_buffer(finfo_open(), base64_decode($Archivo), FILEINFO_MIME_TYPE))[1];

        // $ArchivoGuardado = "ghjghjghjg";

        // $Guardado = $this->Modelo::find($Datos['nombre']);
        // $Guardado->fill(['foto' => $ArchivoTipo]);
        // $Guardado->save();

        // $Dir = 'archivos/' . $Datos['Carpeta'];
        // if(!is_dir($Dir)) mkdir($Dir);
        // $Ruta = $Dir . '/' . $Datos['Nombre'] . '.' . $ArchivoTipo;	
        // if(file_put_contents($Ruta, base64_decode($Archivo)))
        //     return response()->json(['proceso' => true, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
        
        return response()->json(['proceso' => false, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
    }


    function Busqueda(Request $request) {
        $Dato = $request->all()['datos']['Dato'];
        
        $Franquicia = Franquicia::query();
        $Franquicia = $Franquicia->where('nombre', 'LIKE', '%'.$Dato.'%');
        $Franquicia = $Franquicia->orWhere('id', '=', $Dato);
        $Franquicia = $Franquicia->select('*');
        $DatosIns = $Franquicia->get();
        
        $lpf = Modelo::query();
        $lpf = $lpf->where('nombre', 'LIKE', '%'.$Dato.'%');
        $lpf = $lpf->orWhere('apellidos', 'LIKE', '%'.$Dato.'%');
        $lpf = $lpf->orWhere('correo', 'LIKE', '%'.$Dato.'%');
        $lpf = $lpf->orWhere('id', '=', $Dato);
        $lpf = $lpf->select('*');
        $DatosAlu = $lpf->get();

        $Datos = [];
        $i = 0;
        foreach ($DatosIns as $Franquicia) {
            $Datos[$i]= $Franquicia;
            $Datos[$i]['tipo'] = 1;
            $i++;
        }
        foreach ($DatosAlu as $lpf) {
            $Datos[$i]= $lpf;
            $Datos[$i]['tipo'] = 2;
            $i++;
        }

        return response()->json([
            'proceso' => true, 
            'datos' => $Datos
        ]);
    }

    function Dashboard(Request $request) {
        // $Dato = $request->all()['datos']['Dato'];

        $Datos = array();

        //LEADS
        $LEADS_Total = Modelo::where('id_status', '=', 1)
                        ->count();
        $LEADS_Antes = Modelo::where('id_status', '=', 1)
                        ->where('created_at', '<', date('Y-m-d', strtotime("-1 week")))
                        ->count();
        $LEADS_Nuevos = Modelo::where('id_status', '=', 1)
                        ->where('created_at', '>=', date('Y-m-d', strtotime("-1 week")))
                        ->count();
        $Datos['Leads']['total'] = $LEADS_Total;
        $Datos['Leads']['antes'] = $LEADS_Antes;
        $Datos['Leads']['nuevos'] = $LEADS_Nuevos;
        $Datos['Leads']['porcentaje'] = round(($LEADS_Nuevos * 100) / (($LEADS_Antes == 0) ? 1: $LEADS_Antes));

        //PROSPECTOS
        $PROSP_Total = Modelo::where('id_status', '=', 2)
                        ->count();
        $PROSP_Antes = Modelo::where('id_status', '=', 2)
                        ->where('fecha_p', '<', date('Y-m-d', strtotime("-1 week")))
                        ->count();
        $PROSP_Nuevos = Modelo::where('id_status', '=', 2)
                        ->where('fecha_p', '>=', date('Y-m-d', strtotime("-1 week")))
                        ->count();
        $Datos['Prospectos']['aplicaciones_completas'] = Modelo::where('id_status', '=', 2)
                                                            ->whereNotNull('calle_numero')
                                                            ->count();
        $Datos['Prospectos']['aplicaciones_incompletas'] = Modelo::where('id_status', '=', 2)
                                                            ->whereNull('calle_numero')
                                                            ->count();
        $Datos['Prospectos']['aplicaciones_completas_porc'] = round(($Datos['Prospectos']['aplicaciones_completas'] * 100) / (($PROSP_Total == 0) ? 1: $PROSP_Total));
        $Datos['Prospectos']['aplicaciones_incompletas_porc'] = round(($Datos['Prospectos']['aplicaciones_incompletas'] * 100) / (($PROSP_Total == 0) ? 1: $PROSP_Total));
        $Datos['Prospectos']['total'] = $PROSP_Total;
        $Datos['Prospectos']['antes'] = $PROSP_Antes;
        $Datos['Prospectos']['nuevos'] = $PROSP_Nuevos;
        $Datos['Prospectos']['porcentaje'] = round(($PROSP_Nuevos * 100) / (($PROSP_Antes == 0) ? 1: $PROSP_Antes));

        //FRANQUICIATARIOS
        $FRANQ_Total = Modelo::where('id_status', '=', 3)
                        ->count();
        $FRANQ_Antes = Modelo::where('id_status', '=', 3)
                        ->where('fecha_f', '<', date('Y-m-d', strtotime("-1 week")))
                        ->count();
        $FRANQ_Nuevos = Modelo::where('id_status', '=', 3)
                        ->where('fecha_f', '>=', date('Y-m-d', strtotime("-1 week")))
                        ->count();
        $Datos['Franquiciatarios']['aplicaciones_incompletas'] = Modelo::where('id_status', '=', 2)
                                                            ->whereNull('calle_numero')
                                                            ->count();
        $Datos['Franquiciatarios']['aplicaciones_incompletas_porc'] = round(($Datos['Franquiciatarios']['aplicaciones_incompletas'] * 100) / (($FRANQ_Total == 0) ? 1: $FRANQ_Total));

        $Datos['Franquiciatarios']['ingresos_total'] = Usuario::count();
        $Datos['Franquiciatarios']['ingresos_no'] = Usuario::where('ingreso', '<=', date('Y-m-d', strtotime("-2 week")))
                                                    ->count();
        $Datos['Franquiciatarios']['ingresos_porcentaje'] = round(($Datos['Franquiciatarios']['ingresos_no'] * 100) / (($Datos['Franquiciatarios']['ingresos_total'] == 0) ? 1: $Datos['Franquiciatarios']['ingresos_total']));
        $Datos['Franquiciatarios']['total'] = $FRANQ_Total;
        $Datos['Franquiciatarios']['antes'] = $FRANQ_Antes;
        $Datos['Franquiciatarios']['nuevos'] = $FRANQ_Nuevos;
        $Datos['Franquiciatarios']['porcentaje'] = round(($FRANQ_Nuevos * 100) / (($FRANQ_Antes == 0) ? 1: $FRANQ_Antes));

        // $Datos['leads'] = Modelo::where()

        return response()->json([
            'proceso' => true, 
            'datos' => $Datos
        ]);
    }


    /* function Archivos(Request $request) {
        $Datos = "";
        $Datos = $request->all()['datos'];
        $Archivo64 = $request->all()['datos']['Base64'];
        $Archivo = explode(',', $Archivo64)[1];
        
        $ArchivoTipo = explode('/', finfo_buffer(finfo_open(), base64_decode($Archivo), FILEINFO_MIME_TYPE))[1];

        $ArchivoGuardado = TareaArchivo::create(['descripcion' => $Datos['Nombre'], 'extension' => $ArchivoTipo, 'id_tarea' => $Datos['Nombre']]);

        if(!empty($ArchivoGuardado)) {
            $Dir = 'archivos/' . $Datos['Carpeta'];
            if(!is_dir($Dir)) mkdir($Dir);
            $Ruta = $Dir . '/' . $ArchivoGuardado['id'] . '.' . $ArchivoTipo;	
            if(file_put_contents($Ruta, base64_decode($Archivo)))
                return response()->json(['proceso' => true, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
        }
        return response()->json(['proceso' => false, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
    } */



}
