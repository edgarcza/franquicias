<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Modelos\Usuario;
use Auth;

class UsuarioController extends Controller
{
    public function Login(Request $request) {
        $Usuario = $request->all()['datos'];
        if(Auth::attempt($Usuario, true)) {
            $Usuario = Auth::user();
            $Usuario->ingreso = date('Y-m-d');
            $Usuario->save();
            return response()->json(['proceso' => true, 'usuario' => Auth::user()]);
        }
        else {
            return response()->json(['proceso' => false, 'usuario' => $Usuario]);
        }
    }

    public function Sesion(Request $request) {
        $Usuario = Usuario::where('remember_token', $request->all()['datos']['remember_token'])->first();
        if($Usuario)
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    //
    public function Registrar(Request $request) {
        $Usuario = $request->all()['datos'];

        $Creado = Usuario::create([
            'nombres' => $Usuario['nombres'],
            'apellidos' => $Usuario['apellidos'],
            'usuario' => $Usuario['usuario'],
            'password' => Hash::make($Usuario['password']),
        ]);

        if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }
    
    public function Registro(Request $request) {
        $Usuario = $request->all()['datos'];

        $Existente = Usuario::where('usuario', $Usuario['usuario'])->first();
        if(!empty($Existente))
            return response()->json(['proceso' => false, 'usuario' => $Existente, 'error' => 1]);

        $Creado = Usuario::create([
            'nombres' => $Usuario['nombres'],
            'apellidos' => $Usuario['apellidos'],
            'usuario' => $Usuario['usuario'],
            'password' => Hash::make($Usuario['password']),
        ]);

        if(!empty($Creado))
            return response()->json(['proceso' => true, 'usuario' => $Creado]);
        return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    public function Guardar(Request $request) {
        $Datos = $request->all()['datos'];

        // $Usuario = Usuario::find($Datos['id']);
        // $Usuario
        // $Usuario->email = $Datos['email'];
        // $Usuario['password'] = Hash::make($Datos['password']);
        // $Usuario->save();
        // $Usuario;

        $Usuario = Usuario::find($Datos['id']);
        // $Usuario = new Usuario($Datos);
        $Usuario->fill($Datos);
        $Usuario->password = Hash::make($Datos['password']);
        $Usuario->save();

        // if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        // return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->all()['datos'];

        $Usuario = Usuario::where('id', $ID)->delete();
        // $Usuario;

        // if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        // return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    
    public function Usuario(Request $request) {
        $Filtros = $request->all()['datos']['Filtros'];
        $Paginador = $request->all()['datos']['Paginador'];
        
        $Query = Usuario::query();
        foreach ($Filtros as $Filtro) {
            $Query = $Query->where($Filtro['Campo'], 'like', '%'.$Filtro['Valor'].'%');
        }
        $DatosUL = $Query->get()->count();
        $Query = $Query->offset($Paginador['Cantidad'] * $Paginador['Pagina']);
        $Query = $Query->limit($Paginador['Cantidad']);
        $Datos = $Query->get();



        return response()->json(['proceso' => true, 'datos' => $Datos, 'total' => $DatosUL, 'req' => $request->all()['datos']['Paginador']]);
    }
    
    public function Filtrar(Request $request) {
        $Filtros = $request->all()['datos']['Filtros'];
        $Paginador = $request->all()['datos']['Paginador'];
        
        $Query = Usuario::query();

        // if(!empty($this->Joins)) {
        //     foreach ($this->Joins as $Join) {
        //         $Query->join($Join[0], $Join[1], $Join[2]);
        //     }
        // }v
        $Query->join('franquicia', 'usuarios.id_franquicia', 'franquicia.id');

        foreach ($Filtros as $Filtro) {
            $Query = $Query->where($Filtro['Campo'], 'like', '%'.$Filtro['Valor'].'%');
        }

        $Query = $Query->select('*', 'usuarios.id as id_principal');

        $DatosUL = $Query->get()->count();

        if(!empty($Paginador)) {
            $Query = $Query->offset($Paginador['Cantidad'] * $Paginador['Pagina']);
            $Query = $Query->limit($Paginador['Cantidad']);
        }
        $Datos = $Query->get();

        return response()->json(['proceso' => true, 'datos' => $Datos, 'total' => $DatosUL, 'req' => $request->all()['datos']['Paginador']]);
    }
}
