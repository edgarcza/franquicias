<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Modelos\multimedia as Modelo;
use App\Modelos\Upload;

class multimediaController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\multimedia';
    }

    /* function Archivos(Request $request) {
        $Upload = new Upload($request->all()['datos']);

        if($Upload->Guardar($Upload->Nombre)) {
            return response()->json([
                'proceso' => true, 
                'Info' => 'ArchivoGuardado'
            ]);
        }       
        return response()->json(['proceso' => false, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
    } */
    function Archivos(Request $request) {
        $Upload = new Upload($request->all()['datos']);

        // $MM = Modelo::find($Upload->Datos['id']);
        // $MM->foto = $Upload->Extension;
        // $MM->save();

        $MM = new Modelo();
        $MM->Guardar([
            'id_franquicia' => $Upload->Datos['id'],
            'foto' => $Upload->Extension,
        ]);

        if($Upload->Guardar($Upload->Datos['id'] .'_'. $MM->Datos['id'] )) {
            return response()->json([
                'proceso' => true, 
                'Info' => $Upload
            ]);
        }
        return response()->json(['proceso' => false, 'datos' => $Archivo, '64' => $Archivo64, 'Info' => $ArchivoGuardado]);
    }
}
