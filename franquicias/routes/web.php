<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/get', function () {
    // return 
    // return response()->json(["prueba" => "probando"]);
});

Route::post('/post', function (Request $request) {
    return response()->json(["res" => csrf_token()]);
});

Route::get('/token', function () {
    return response()->json(["token" => csrf_token()]);
});

Route::post('/token', 'UsuarioController@prueba');
Route::post('/token', 'UsuarioController@prueba');

Route::post('/usuarios', 'UsuarioController@Filtrar');

Route::post('/login', 'UsuarioController@Login');
Route::post('/sesion', 'UsuarioController@Sesion');
Route::post('/registrar', 'UsuarioController@Registrar');
Route::post('/registro', 'UsuarioController@Registro');

Route::post('/usuario/eliminar', 'UsuarioController@Eliminar');
Route::post('/usuario/guardar', 'UsuarioController@Guardar');

Route::post('/usuario', 'UsuarioController@Usuario');

Route::post('/institucion/guardar', 'InstitucionController@Guardar');
Route::post('/institucion/eliminar', 'InstitucionController@Eliminar');
Route::post('/instituciones', 'InstitucionController@Filtrar');
Route::get('/instituciones', 'InstitucionController@Todas');


Route::post('/franquicias', 'FranquiciaController@Filtrar');
Route::post('/franquicias/eliminar', 'FranquiciaController@Eliminar');
Route::post('/franquicias/guardar', 'FranquiciaController@Guardar');
Route::get('/franquicias', 'FranquiciaController@Todas');

Route::post('/lpf', 'lpfController@Filtrar');
Route::post('/lpf/eliminar', 'lpfController@Eliminar');
Route::post('/lpf/guardar', 'lpfController@Guardar');
Route::post('/lpf/busqueda', 'lpfController@Busqueda');
Route::post('lpf/archivo', 'lpfController@Archivos');
Route::post('lpf/dashboard', 'lpfController@Dashboard');


Route::post('/usuarioarchivos', 'UsuarioArchivoController@Filtrar');


Route::post('documentos/archivo', 'documentosController@Archivos');
Route::post('documentos', 'documentosController@Filtrar');
Route::post('documentos/eliminar', 'documentosController@Eliminar');
Route::post('/documentos/guardar', 'documentosController@Guardar');

Route::post('multimedia/archivo', 'multimediaController@Archivos');
Route::post('multimedia', 'multimediaController@Filtrar');
Route::post('multimedia/eliminar', 'multimediaController@Eliminar');

Route::post('/directorio', 'directorioController@Filtrar');
Route::post('/directorio/guardar', 'directorioController@Guardar');


Route::post('/calendario', 'calendarioController@Filtrar');
Route::post('/calendario/guardar', 'calendarioController@Guardar');
Route::post('/calendario/institucion', 'calendarioController@Institucion');
Route::post('/calendario/instituciondb', 'calendarioController@InstitucionDB');
Route::post('/calendario/eliminar', 'calendarioController@Eliminar');




Route::post('/directorio/eliminar', 'directorioController@Eliminar');