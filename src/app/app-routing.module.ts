import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './inicio/inicio.component';
import { LeadsComponent } from './leads/leads.component';
import { ProspectosComponent } from './prospectos/prospectos.component';
import { FranquiciatariosComponent } from './franquiciatarios/franquiciatarios.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegistroComponent } from './login/registro/registro.component';
import { UsuariosComponent } from './inicio/usuarios/usuarios.component';
import { FranquiciasComponent } from './franquicias/franquicias.component';
import { DatosFranquiciaComponent } from './datos-franquicia/datos-franquicia.component';
import { DatosPersonaComponent } from './datos-persona/datos-persona.component';

const routes: Routes = [
  { 
    path: '', redirectTo: '/Entrar', pathMatch: 'full' 
  },
  { 
    path: 'Entrar', component: LoginComponent,
  },
  { 
    path: 'Registro', component: RegistroComponent,
  },
  { 
    path: 'Inicio', component: InicioComponent,
    children: 
    [
      {
        path: 'Leads', component: LeadsComponent
      },
      {
        path: 'Prospectos', component: ProspectosComponent
      },
      {
        path: 'Franquiciatarios', component: FranquiciatariosComponent
      },
      {
        path: 'Dashboard', component: DashboardComponent
      },
      
      {
        path: 'Usuarios', component: UsuariosComponent
      },
      {
        path: 'Franquicias', component: FranquiciasComponent
      },
      {
        path: 'DatosFranquicia/:id', component: DatosFranquiciaComponent
      },
      {
        path: 'DatosPersona/:id', component: DatosPersonaComponent
      },
    ] 
  },
  { 
    path: 'Entrar', component: LoginComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
