import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-columnas',
  templateUrl: './columnas.component.html',
  styleUrls: ['./columnas.component.css']
})
export class ColumnasComponent implements OnInit {

  constructor(
    public DRef: MatDialogRef<ColumnasComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any
  ) {}

  ngOnInit() {
    console.log(this.DialogDatos);
  }

  Cerrar() {
    this.DRef.close("dsad");
  }
}
