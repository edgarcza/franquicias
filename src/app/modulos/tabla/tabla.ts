import * as XLSX from 'xlsx';
import { HttpService } from '../../servicios/http.service';

export interface TablaConfig {
	Filtros?: any[],
	Paginador?: {
			Pagina?: number,
			Cantidad?: number,
			Total?: number,
  },
  Tabla?: {
    Nombre?: string,
    Campos?: any[],
    Acciones?: any[]
  }
}

export class Tabla{
	Configuracion: TablaConfig = {Filtros: [], Paginador: {Cantidad: 10, Pagina: 0, Total: 0}};
  public Datos = {Campos: [], Filas: []};
  private PreFiltros = [];    
  constructor(public HTTP: HttpService) {
  this.Configuracion = {Filtros: [], Paginador: {Cantidad: 10, Pagina: 0, Total: 0}};
	}

  /**
   * 
   * @param datos 
   * {Paginador: {Pagina, Cantidad, Total}, 
   * Tabla: {
   *  Nombre, 
   *  Campos: {Id, Nombre, Campo, Oculto, Total}, 
   *  Acciones: {Nombre, Tipo, Accion}
   * }
   * }
   * @param prefiltros 
   * [{Campo, Valor, Condicion}]
   */
	ConfigurarTabla(datos: TablaConfig, prefiltros = []) {
    Object.assign(this.Configuracion, datos);
    Object.assign(this.Datos, this.Configuracion.Tabla);

    Object.assign(this.PreFiltros, prefiltros);

    console.log(this.Configuracion);
    this.Tabla();
	}

	Tabla() {
    let Filtros = [];
    for(var F in this.PreFiltros)
      Filtros.push(this.PreFiltros[F]);

    for(var F in this.Configuracion.Filtros)
      if(this.Configuracion.Filtros[F].Filtrar)
        Filtros.push(this.Configuracion.Filtros[F]);
        
    this.HTTP.post(this.Configuracion.Tabla.Nombre, {Filtros: Filtros, Paginador: this.Configuracion.Paginador}).subscribe((respuesta: any) => {
      console.log(respuesta);
      this.Configuracion.Paginador.Total = respuesta.total;
      // this.Datos.Filas = respuesta.datos;
      this.Datos.Filas = respuesta.datos;
      this.Datos = Object.assign({}, this.Datos);
      // console.log(this.Datos);
    })
		// this.Datos = 
    // {
    //   Campos: 
    //   [
    //     {Id: 'campo1',   Nombre: 'Fecha de venta',       Ocultar: false,    Campo: 'cotizacion.campo1', Total: false},
    //     {Id: 'campo2',   Nombre: 'Fecha de venta 2',     Ocultar: false,    Campo: 'cotizacion.campo2', Total: false},
    //     {Id: 'campo3',   Nombre: 'Fecha de venta 3',     Ocultar: false,    Campo: 'cotizacion.campo3', Total: true},  
    //     {Id: 'campo4',   Nombre: 'Fecha de venta 4',     Ocultar: false,    Campo: 'cotizacion.campo4', Total: false},
    //   ],
    //   Filas:
    //   [
    //     {
    //       Id: 1,
    //       campo1: 'Valor 1 campo 1',
    //       campo2: 'Valor 1 campo 2',
    //       campo3: '1541',
    //       campo4: 'Valor 1 campo 4'
    //     },    
    //     {
    //       Id: 8,
    //       campo1: 'Valor 2 campo 1',
    //       campo2: 'Valor 2 campo 2',
    //       campo3: '1454',
    //       campo4: 'Valor 2 campo 4'
    //     },    
    //     {
    //       Id: 5,
    //       campo1: 'Valor 3 campo 1',
    //       campo2: 'Valor 3 campo 3',
    //       campo3: '7843',
    //       campo4: 'Valor 3 campo 4'
    //     }
    //   ],
    //   Acciones:
    //   [
    //     {
    //       Nombre: 'Acción link', Tipo: 'link', Accion: '/Inicio/Institucion/1/Portafolio'
    //     },
    //     {
    //       Nombre: 'Acción interna', Tipo: 'interna', Accion: 'Ver'
    //     },
    //     {
    //       Nombre: 'Acción interna2', Tipo: 'interna', Accion: 'ventana'
    //     }
    //   ]
		// };
    // for(var F in this.Datos.Filas)
    // {
    //   this.AccionesVer[this.Datos.Filas[F].id] = false;
    //   this.SeleccionFilas[F] = false;
		// }
    // this.Configuracion.Filtros = [];
    if(this.Datos.Campos.length > 0)
      if(this.Configuracion.Filtros.length == 0)
        for(var F in this.Datos.Campos)
        {
          this.Configuracion.Filtros.push({Filtrar: false, Campo: this.Datos.Campos[F].Campo, Nombre: this.Datos.Campos[F].Nombre, Valor: ""})
        }
		// console.log(this.Configuracion.Filtros)
				
	}

	Evento(caso) {
		if(caso === "Excel") {
			const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.Datos.Filas);
	
			/* generate workbook and add the worksheet */
			const wb: XLSX.WorkBook = XLSX.utils.book_new();
			XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');
	
			/* save to file */
			XLSX.writeFile(wb, 'Excel.xlsx');
		}
	}

   
}
