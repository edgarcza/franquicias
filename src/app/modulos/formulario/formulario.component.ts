import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from 'src/app/servicios/http.service';
import { HttpHeaders } from '@angular/common/http';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from './FechaAdaptador';

@Component({
  selector: 'formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'mx'},
  ]
})
export class FormularioComponent implements OnInit {

  @Output() enviar = new EventEmitter();  
  @Output() construido = new EventEmitter();  
  @Input('form') Grupo: FormGroup;
  @Input('controles') Controles;
  @Input('predems') Predems;
  @Input('opciones') Opciones;
  @Input('diseno') Diseno = false;
  @Input('col') Col = 12;

  Cols = [];
  ColExtT = 0;
  Opcion = 0;

  constructor(private Http: HttpService) { }

  ngOnInit() {
    this.Construir();
  }

  ngOnChanges(changes: SimpleChanges) {
    // const name: SimpleChange = changes.name;
    // console.log('prev value: ', name.previousValue);
    // console.log('got name: ', name.currentValue);
    // this._name = name.currentValue.toUpperCase();
    this.Construir();
  }

  Construir() {
    if(this.Col < 12) {
      this.ColExtT = (12 - this.Col) / 2;
    }

    for(var N in this.Controles) {
      // console.log(this.Controles[N]);
      this.Grupo.addControl(this.Controles[N].Nombre, new FormControl(this.Controles[N].Default, this.Controles[N].Validadores));
    }

    // console.log(this.Grupo.value);
    if(this.Predems)
      this.Grupo.patchValue(this.Predems);

    this.construido.emit(1);
  }
  
  Submit() {
    this.enviar.emit(this.Opcion);
  }

  CambiarOpcion(o) {
    // this.Opcion = (this.Opciones.length) - o;
    this.Opcion = o + 1;
    // console.log("Opcion: ", this.Opcion);
  }

  Cambio(e, nom) {
    // console.log(e);
    if(nom === "cp") {
      // if(this.Controles)
      let CP = this.Grupo.get(nom).value;
      if(!CP) return;
      // console.log(CP) 
      if(CP.length > 2) {
        this.Http.getUrl('https://api-codigos-postales.herokuapp.com/v2/codigo_postal/' + CP, 
        new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded')).subscribe((respuesta: any) => {
          // console.log(respuesta);
          try {
            this.Grupo.get("estado").setValue(respuesta.estado);
            this.Grupo.get("ciudad").setValue(respuesta.municipio);
            // this.Controles
          }
          catch(e) {

          }
        })
      }
    }
  }

  // Validadores(vals)
  // {

  // }

}
