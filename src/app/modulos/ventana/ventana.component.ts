import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-ventana',
  templateUrl: './ventana.component.html',
  styleUrls: ['./ventana.component.css']
})
export class VentanaComponent implements OnInit {

  constructor(
    public DRef: MatDialogRef<VentanaComponent>,
    @Inject(MAT_DIALOG_DATA) public DialogDatos: any
  ) { }

  ngOnInit() {
    console.log(this.DialogDatos);
  }

  Cerrar() {
    // this.DRef.close();
  }

}
