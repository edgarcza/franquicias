import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { VentanaService } from '../servicios/ventana.service';
import { LeadsComponent } from '../leads/leads.component';
import { Router } from '@angular/router';
import { SesionService } from '../servicios/sesion.service';
import { LeadsNuevoComponent} from '../leads/leads-nuevo/leads-nuevo.component'
import { HttpService } from '../servicios/http.service';
import { Formulario } from '../modulos/formulario/formulario'
import {  FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss'],
}
  )

export class InicioComponent implements OnInit {



 

  Controles = [
    new Formulario().Campo('id', 'id', null, [], 12),
    new Formulario().Campo('Nombres', 'nombres', 'text', [Validators.required], 12),
    new Formulario().Campo('Apellidos', 'apellidos', 'text', [Validators.required], 12),
    new Formulario().Campo('Correo electrónico', 'usuario', 'text', [Validators.required, Validators.email], 12),
    new Formulario().Campo('Contraseña','password','password',[Validators.required], 12),
    new Formulario().Campo('Confirmar contraseña','password2','password',[Validators.required], 12),
  ];

  public InicioForm = new FormGroup({});
  public BusquedaTexto = "";
  public BusquedaResultados: any = [];

  @ViewChild(LeadsComponent) Leads:LeadsComponent;
  public ChildEstado = {
    Leads: false,
  }
  public SideNav = false;


  constructor(private Router: Router, private Ventana: VentanaService, private Http: HttpService,public Sesion: SesionService) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    
  }

  Prueba()
  {
    console.log(this. Sesion.Usuario.tipo);
  }
  AddClick()
  {
  }
  Busqueda(e) {
    if(this.BusquedaTexto.length < 2) {
      this.BusquedaResultados = [];
      return;
    };
    console.log(this.BusquedaTexto);
    this.Http.post("lpf/busqueda", {Dato: this.BusquedaTexto}).subscribe((res: any) => {
      console.log(res);
      this.BusquedaResultados = res.datos;
    })
  }
  ResultadoSeleccionado() {
    this.BusquedaResultados = [];
  }

  ADD() {    
   
    const { url } = this.Router;
    console.log(url);
    if(url === "/Inicio/Leads") {
      this.Ventana.Abrir(LeadsNuevoComponent, {Ancho: '40%', Datos: null}).subscribe(
        respuesta => {
          // console.log(respuesta);
          if(respuesta == 2)
            this.ADD();
        }
      );
    }
  }
  CerrarSesion() {
    localStorage.removeItem("ss");
    this.Sesion.Verificar();
  }
  prueba()
  {
    console.log();
  }
}
