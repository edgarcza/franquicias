import { Component, OnInit, Input } from '@angular/core';
import { Tabla } from '../../modulos/tabla/tabla'
import { VentanaService } from '../../servicios/ventana.service'
import { HttpService } from '../../servicios/http.service';
import { AlertasService } from '../../servicios/alertas.service';
import { SesionService } from '../../servicios/sesion.service'
import { Formulario } from '../../modulos/formulario/formulario'
import { FormGroup, Validators } from '@angular/forms';
import { UsuarioDatosComponent } from './usuario-datos/usuario-datos.component';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent extends Tabla implements OnInit {

  Valores= this.Datos.Filas   ;

  Controles = [
    new Formulario().Campo('id', 'id', null, [], 12),
    new Formulario().Campo('Nombres', 'nombres', 'text', null, 6), 
    new Formulario().Campo('Apellidos', 'apellidos', 'text', null, 4),
    new Formulario().Campo('Usuario', 'usuario', 'text', [Validators.required], 4),
    new Formulario().Campo('Franquicia', 'id_franquicia', 'select', [Validators.required], 6,
    [
      { Valor: "1", Nombre: "Franquicia 1" },
      { Valor: "2", Nombre: "Franquicia 2" },
      { Valor: "3", Nombre: "Franquicia 3" },
    ]
    ), 
    new Formulario().Campo('Contraseña', 'password', 'password', [Validators.required], 4),
    new Formulario().Campo('Tipo_usuario', 'tipo', 'select', [Validators.required], 4,
    [
      { Valor: "1", Nombre: "Administrador" },
      { Valor: "2", Nombre: "Franquiciatario" },
      { Valor: "3", Nombre: "Soporte" },
    ]
    )
  ];

  public UsuariosForm = new FormGroup({});

  constructor(
     private Ventana: VentanaService, 
    public HTTP: HttpService, 
    private Alertas: AlertasService,
    public Sesion: SesionService
    ) { 
      super(HTTP);
    }

  ngOnInit() {
    this.ConfigurarTabla(
      {
        Tabla:
        {
          Nombre: "usuarios", Campos: [
            {
              Id: "id_franquicia", Nombre: "id_franquicia", Ocultar: true, Campo: "id_franquicia", Total: false
              },
              {
                Id: "password", Nombre: "password", Ocultar: true, Campo: "password", Total: false
                },
            {
            Id: "nombres", Nombre: "Nombres", Ocultar: false, Campo: "nombres", Total: false
            },
            {
              Id: "apellidos", Nombre: "Apellidos", Ocultar: false, Campo: "apellidos", Total: false
              },
              {
                Id: "usuario", Nombre: "Usuario", Ocultar: false, Campo: "usuario", Total: false
                }
                ,
                {
                  Id: "nombre", Nombre: "Franquicia", Ocultar: false, Campo: "nombre", Total: false
                }
          ], Acciones: [{
            Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
          }
          ,
          {
            Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
          }]
        }
      }
    )
  }
  Accion(arg) {
    console.log(arg);
    
    console.log("Modificar");
    if (arg.Accion == "Modificar") 
    {
      console.log(this.Valores);

       arg.Datos.password = null;
    this.Ventana.Abrir(UsuarioDatosComponent, { Ancho: '90%',Datos: arg.Datos }).subscribe(
      respuesta => {
        console.log(respuesta);
        if (respuesta == 1)
          this.Tabla();
      });
    } 
    else if (arg.Accion == "Eliminar") 
    {
      console.log("Eliminar");
      this.HTTP.post("usuario/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Usuario eliminado", "Cerrar");
          this.Tabla();
        }})
       }
  }
  Guardar(op)
  {
    {
      console.log(this.UsuariosForm.value);
      console.log("GUARDAR", op);
      let {value} = this.UsuariosForm;
      var franquiciatario =value;
     /*  var Fecha = new Date(value.fecha_nacimiento);
      franquiciatario.fecha_nacimiento = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`; */
      this.HTTP.post("usuario/guardar", franquiciatario).subscribe((respuesta: any) => {
        console.log(respuesta);
        if(respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        }})}
  }
  Agregar() {    
    
    this.Ventana.Abrir(UsuarioDatosComponent, {Ancho: '60%', Datos: null}).subscribe(
      respuesta => {
        console.log(respuesta);
        if(respuesta == 1) {
          this.Tabla();
        }
        else if(respuesta == 2) {
          this.Tabla();
          this.Agregar();
        }
      }
    );
  }
}
