import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario'
import { HttpService } from '../../../servicios/http.service';
import { AlertasService } from '../../../servicios/alertas.service';
import { UsuariosComponent } from '../usuarios.component';

@Component({
  selector: 'app-usuario-datos',
  templateUrl: './usuario-datos.component.html',
  styleUrls: ['./usuario-datos.component.css']
})
export class UsuarioDatosComponent implements OnInit {

  Controles = [
    new Formulario().Campo('id', 'id', null, [], 12),
    new Formulario().Campo('Nombres', 'nombres', 'text', null, 6), 
    new Formulario().Campo('Apellidos', 'apellidos', 'text', null, 4),
    new Formulario().Campo('Usuario', 'usuario', 'text', [Validators.required], 4),
    new Formulario().Campo('Franquicia', 'id_franquicia', 'select', [Validators.required], 6,
    [
      { Valor: 1, Nombre: "Franquicia 1" },
      { Valor: 2, Nombre: "Franquicia 2" },
      { Valor: 3, Nombre: "Franquicia 3" },
    ]
    ), 
    new Formulario().Campo('Contraseña', 'password', 'password', [Validators.required], 4),
    new Formulario().Campo('Tipo_usuario', 'tipo', 'select', [Validators.required], 4,
    [
      { Valor: 1, Nombre: "Administrador" },
      { Valor: 2, Nombre: "Franquiciatario" },
      { Valor: 3, Nombre: "Soporte" },
    ]
    )
  ];

public UsuarioDatosForm = new FormGroup({});

  constructor( public dialogRef: MatDialogRef<UsuarioDatosComponent>,
    @Inject(MAT_DIALOG_DATA) public DDatos: any,
     private HTTP: HttpService,
      private Alertas: AlertasService) { }

      cerrar(): void {
        this.dialogRef.close();
      }


  ngOnInit() {
  }
  Cerrar(caso = 1) {
    this.dialogRef.close(caso);
  }

  Guardar(op)
  {
    let datos = this.UsuarioDatosForm.value;
    datos.id = this.DDatos.id_principal;
    console.log("GUARDAR", op);
    this.HTTP.post("usuario/guardar", datos).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        this.Cerrar(op);
      }})}
}
