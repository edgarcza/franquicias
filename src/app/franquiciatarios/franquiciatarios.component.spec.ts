import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatariosComponent } from './franquiciatarios.component';

describe('FranquiciatariosComponent', () => {
  let component: FranquiciatariosComponent;
  let fixture: ComponentFixture<FranquiciatariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
