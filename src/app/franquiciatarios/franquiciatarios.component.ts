import { Component, OnInit, Input } from '@angular/core';
import { Tabla } from '../modulos/tabla/tabla'
import { VentanaService } from '../servicios/ventana.service'
import { HttpService } from '../servicios/http.service';
import { FranquiciatarioComponent } from './franquiciatario/franquiciatario.component';
import { AlertasService } from '../servicios/alertas.service';
import { FranquiciatariosNuevoComponent } from '../franquiciatarios/franquiciatarios-nuevo/franquiciatarios-nuevo.component';
import { SesionService } from '../servicios/sesion.service';


@Component({
  selector: 'app-franquiciatarios',
  templateUrl: './franquiciatarios.component.html',
  styleUrls: ['./franquiciatarios.component.css']
})
export class FranquiciatariosComponent extends Tabla implements OnInit {


  constructor(private Ventana: VentanaService, HTTP: HttpService, private Alertas: AlertasService, private Sesion: SesionService) {
    super(HTTP);
  }

  ngOnInit() {
    
    this.ConfigurarTabla(
      {
        Tabla: {
          Nombre: "lpf", Campos: [
            {
              Id: "nombre", Nombre: "Nombre", Ocultar: false, Campo: "nombre", Total: false
            },
            {
              Id: "apellidos", Nombre: "Apellidos", Ocultar: false, Campo: "apellidos", Total: false
            },
            {
              Id: "whatsapp", Nombre: "Whatsapp", Ocultar: false, Campo: "whatsapp", Total: false
            },
            {
              Id: "correo", Nombre: "Correo", Ocultar: false, Campo: "correo", Total: false
            },
            {
              Id: "estado", Nombre: "Estado", Ocultar: false, Campo: "estado", Total: false
            },
            {
              Id: "ciudad", Nombre: "Ciudad", Ocultar: false, Campo: "ciudad", Total: false
            },
            {
              Id: "cp", Nombre: "Código Postal", Ocultar: false, Campo: "cp", Total: false
            },
            {
              Id: "fuente_referencia", Nombre: "Fuente Referencia", Ocultar: false, Campo: "lpf.fuente_referencia", Total: false
            }
          ],
          Acciones:
            [
              {
                Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
              }
              ,
              {
                Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
              }]
        }
      },
      [
        { Campo: 'id_status', Valor: '3', Condicion: '=' },
      ]);
 
    if (this.Sesion.Usuario.tipo && this.Sesion.Usuario.tipo != 1) {
      this.ConfigurarTabla(null,
      [
        { Campo: 'id_status', Valor: '3', Condicion: '=' },
        { Campo: 'id_franquicia', Valor: this.Sesion.Usuario.id_franquicia, Condicion: '=' }
      ]);
    }


    this.Sesion.Verificado.subscribe(Vari => {
      if (this.Sesion.Usuario.tipo != 1)
        this.ConfigurarTabla(
          null,
          [
            { Campo: 'id_status', Valor: '3', Condicion: '=' },
            { Campo: 'id_franquicia', Valor: this.Sesion.Usuario.id_franquicia, Condicion: '=' }
          ])
    })
  }
  Accion(arg) {
    console.log(arg);
    if (arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(FranquiciatarioComponent, { Ancho: '90%', Datos: arg.Datos }).subscribe(
        respuesta => {
          console.log(respuesta);
          this.Tabla();
        });
    }
    else if (arg.Accion == "Eliminar") {
      this.HTTP.post("lpf/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Registro eliminado", "Cerrar");
          this.Tabla();
        }
      })
    }
  }
  Agregar() {
    this.Ventana.Abrir(FranquiciatariosNuevoComponent, { Ancho: '60%', Datos: null }).subscribe(
      respuesta => {
        console.log(respuesta);
        if (respuesta == 2) {
          this.Tabla();
        }
        else if (respuesta == 1) {
          this.Tabla();
          this.Agregar();
        }
      }
    );
  }
}
