import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatariosNuevoComponent } from './franquiciatarios-nuevo.component';

describe('FranquiciatariosNuevoComponent', () => {
  let component: FranquiciatariosNuevoComponent;
  let fixture: ComponentFixture<FranquiciatariosNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatariosNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatariosNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
