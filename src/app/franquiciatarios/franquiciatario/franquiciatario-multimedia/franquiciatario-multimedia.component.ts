import { Component, OnInit, Inject } from '@angular/core';
import { HttpService } from 'src/app/servicios/http.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UploadComponent } from 'src/app/modulos/upload/upload.component';
import { UploadDatos } from 'src/app/modulos/upload/upload';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-franquiciatario-multimedia',
  templateUrl: './franquiciatario-multimedia.component.html',
  styleUrls: ['./franquiciatario-multimedia.component.css']
})
export class FranquiciatarioMultimediaComponent implements OnInit {
  Imagenes = [];
  elim = "a";
  public SeleccionMultiple = false;
  public AIMGPRUEBA = [false, false, false, false, false, false];
  public MultimediaEstilo = 1; // 1 cols, 2 list

  constructor(public HTTP: HttpService,
    @Inject(MAT_DIALOG_DATA) public Datos: any,
    public Ventana: VentanaService,
    public Alertas : AlertasService) { }

  ngOnInit() {
    this.CargarMultimedia();
    this.MultimediaSeleccion;
  }
 
  CargarMultimedia() {
    this.HTTP.post("multimedia", { Filtros: [{ Campo: "id_franquicia", Valor: this.Datos.id_franquicia, Condicion: "=" }], Paginador: null }).subscribe((Docs: any) => {
      console.log(Docs);
      this.Imagenes = Docs.datos;
      for (var i in this.Imagenes) {
        this.Imagenes[i].seleccionada = false;
      }
    });
  }

  AgregarArchivo1() {
    let asd: UploadDatos = {
      Carpeta: 'multimedia',
      Datos: { id: this.Datos.id_franquicia },
      Descripcion: true,
      Multiple: true,
      Nombres: false,
      Ruta: 'multimedia/archivo',
    };

    console.log({});
    this.Ventana.Abrir(UploadComponent, { Ancho: '50%', Datos: asd }).subscribe((res) => {
      console.log(res);
      this.CargarMultimedia();
      if (res == 1) {
      }
    })

  }

  MultimediaSeleccion(val) {
    this.SeleccionMultiple = val;
  }

  MultimediaClickImagen(val) {
    if (!this.SeleccionMultiple) {
      // Abrir
    }
    else {
      this.Imagenes[val].seleccionada = !this.Imagenes[val].seleccionada;
 
      // if(this.AIMGPRUEBA[val] == false)
      //   this.AIMGPRUEBA[val] = true;
      // else
      //   this.AIMGPRUEBA[val] = false;
    }
    console.log("asd");
  }

  MultimediaSubir() {

  }

  MultimediaEliminar() {
    for (var i in this.Imagenes) 
    {

      if(this.Imagenes[i].seleccionada == 1 )
      {
      this.elim = this.Imagenes[i].id_franquicia + "_" + this.Imagenes[i].id_principal;
     
      console.log(this.elim);
     this.HTTP.post("multimedia/eliminar", this.Imagenes[i].id_principal).subscribe((respuesta: any) => {
      if (respuesta.proceso) {
       
      }
    }) 

  }


  } 
  this.CargarMultimedia();
  this.Alertas.MSB_Mostrar("Imágenes eliminadas", "Cerrar");

  
  }

  MultimediaColumnas() {
    this.MultimediaEstilo = 1;
  }

  MultimediaLista() {
    this.MultimediaEstilo = 2;
  }


}
