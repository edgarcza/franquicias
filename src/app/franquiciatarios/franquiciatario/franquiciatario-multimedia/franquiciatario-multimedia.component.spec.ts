import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatarioMultimediaComponent } from './franquiciatario-multimedia.component';

describe('FranquiciatarioMultimediaComponent', () => {
  let component: FranquiciatarioMultimediaComponent;
  let fixture: ComponentFixture<FranquiciatarioMultimediaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatarioMultimediaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatarioMultimediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
