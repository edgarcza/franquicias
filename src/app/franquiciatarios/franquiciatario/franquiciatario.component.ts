import { Component, OnInit, Inject } from '@angular/core';
import { VentanaService } from './../../servicios/ventana.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { from } from 'rxjs';
import { UploadComponent } from 'src/app/modulos/upload/upload.component';
import { SesionService } from '../../servicios/sesion.service';

@Component({
  selector: 'app-franquiciatario',
  templateUrl: './franquiciatario.component.html',
  styleUrls: ['./franquiciatario.component.css']
})
export class FranquiciatarioComponent implements OnInit {

  Tab = 0;
  constructor(
    public dialogRef: MatDialogRef<FranquiciatarioComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any,private Ventana: VentanaService,public Sesion: SesionService
    // public Formulario: DatosFormularioService
  ) { }

  cerrar(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }
  AgregarArchivo() {
    this.Ventana.Abrir(UploadComponent, {Ancho: '50%', Datos: {Ruta: 'lpf/archivo', Carpeta: 'lpf', Nombre: this.Datos.id}}).subscribe((res) => {
      console.log(res);
   
    })
    // // console.log(this.Archivos.forEach((e) => {console.log(e)}));
    // this.Archivo.nativeElement.click();
  }

}
