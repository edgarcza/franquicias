import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatarioComponent } from './franquiciatario.component';

describe('FranquiciatarioComponent', () => {
  let component: FranquiciatarioComponent;
  let fixture: ComponentFixture<FranquiciatarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
