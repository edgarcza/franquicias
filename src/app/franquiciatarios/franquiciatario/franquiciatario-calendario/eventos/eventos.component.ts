import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../../modulos/formulario/formulario'
import { HttpService} from '../../../../servicios/http.service';
import { AlertasService } from '../../../../servicios/alertas.service';
import { VentanaService } from 'src/app/servicios/ventana.service';
import {Tabla} from '../../../../modulos/tabla/tabla'

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent extends Tabla implements OnInit {

 

  public Eventos = new FormGroup({});

  constructor(
    public dialogRef: MatDialogRef<EventosComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any,
     private Ventana: VentanaService,
    // public Formulario: DatosFormularioService
  public HTTP: HttpService, private Alertas: AlertasService)
  
  { 
    super(HTTP);
  }

  ngOnInit() {
    let idfran:string = this.Datos.id_franquicia;
    this.ConfigurarTabla(
      {
        Tabla:{
          Nombre:"calendario",Campos:[
            {
              Id:"id", Nombre: "ID", Ocultar: true, Campo: "id", Total: false
            }
            ,
            {
              Id:"titulo" , Nombre: "Título", Ocultar: false, Campo:"titulo", Total: false
            },
            {
              Id:"fecha_inicio" , Nombre: "Fecha de inicio", Ocultar: false, Campo:"fecha_inicio", Total: false
            },
            {
              Id:"hora_inicio" , Nombre: "Hora de inicio", Ocultar: false, Campo:"hora_inicio", Total: false
            },
            {
              Id:"descripcion" , Nombre: "Descripción", Ocultar: false, Campo:"descripcion", Total: false
            },
            {
              Id: "id_franquicia", Nombre: "Id_franquicia", Ocultar: true, Campo: "id_franquicia", Total: false
            }
          ],
          Acciones:
          [
            {
              Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
            }
            ,
            {
              Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
            }]}},
          [{Campo:'id_franquicia',Valor:idfran,Condicion:'='}])
  }

  Accion(arg) {
    /* 
    console.log(arg);
    if (arg.Accion == "Modificar") {
      arg.Datos.password = null;
    this.Ventana.Abrir(FranquiciatarioComponent, { Ancho: '90%',Datos: arg.Datos }).subscribe(
      respuesta => {
        console.log(respuesta);
        //if (respuesta == 1)
          this.Tabla();
      });
    }
    else if (arg.Accion == "Eliminar") {
      this.HTTP.post("lpf/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Registro eliminado", "Cerrar");
          this.Tabla();
        }
      })
    } */
}


}
