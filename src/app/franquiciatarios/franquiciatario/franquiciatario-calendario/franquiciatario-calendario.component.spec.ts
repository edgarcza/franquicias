import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatarioCalendarioComponent } from './franquiciatario-calendario.component';

describe('FranquiciatarioCalendarioComponent', () => {
  let component: FranquiciatarioCalendarioComponent;
  let fixture: ComponentFixture<FranquiciatarioCalendarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatarioCalendarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatarioCalendarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
