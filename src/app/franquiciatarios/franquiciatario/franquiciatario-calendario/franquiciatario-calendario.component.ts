import { Component, ViewChild ,OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario'
import { HttpService} from '../../../servicios/http.service';
import { AlertasService } from '../../../servicios/alertas.service';
import { VentanaService } from 'src/app/servicios/ventana.service';
import { ActivatedRoute } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeEsMX from '@angular/common/locales/es-MX';
registerLocaleData(localeEsMX, 'es-MX');

@Component({
  selector: 'app-franquiciatario-calendario',
  templateUrl: './franquiciatario-calendario.component.html',
  styleUrls: ['./franquiciatario-calendario.component.css']
})

export class FranquiciatarioCalendarioComponent implements OnInit {

public hoy = new Date();
  public fecha_hoy = new Date().getTime();
  


  public Procesando = true;
  public Tareas = [];
  public Calendario = [];
  public DUsuarios = [];

  @ViewChild('Archivo') Archivo;

  CalendarioControles = [
    new Formulario().Campo('Título', 'titulo', 'text', [Validators.required], 12),
    new Formulario().Campo('Fecha inicio', 'fecha_inicio', 'date', [Validators.required], 6),
    new Formulario().Campo('Hora inicio', 'hora_inicio', 'select', [Validators.required], 6,
    [
      {Valor:"07",Nombre:"7 a.m."},
      {Valor:"08",Nombre:"8 a.m."},
      {Valor:"09",Nombre:"9 a.m."},
      {Valor:"10",Nombre:"10 a.m."},
      {Valor:"11",Nombre:"11 a.m."},
      {Valor:"12",Nombre:"12 p.m."},
      {Valor: "13", Nombre: " 1 p.m."},
      {Valor: "14", Nombre: "2 p.m."},
      {Valor: "15", Nombre: "3 p.m."},
      {Valor:"16",Nombre:"4 p.m."},
      {Valor:"17",Nombre:"5 p.m."},
      {Valor:"18",Nombre:"6 p.m."},
      {Valor:"19",Nombre:"7 p.m."},
      {Valor:"20",Nombre:"8 p.m."},
      {Valor:"21",Nombre:"9 p.m."},
      {Valor:"22",Nombre:"10 p.m."},
      {Valor:"23",Nombre:"11 p.m."},
      {Valor:"24",Nombre:"12 a.m."},
      {Valor: "01", Nombre: " 1 a.m."},
      {Valor: "02", Nombre: "2 a.m."},
      {Valor: "03", Nombre: "3 a.m."},
      {Valor:"04",Nombre:"4 a.m."},
      {Valor:"05",Nombre:"5 a.m."},
      {Valor:"06",Nombre:"6 a.m."},
      
    ]), new Formulario().Campo('Minuto', 'minuto_inicio', 'select', [Validators.required], 6,
    [
      {Valor:"00", Nombre: "00"},
      {Valor:"01", Nombre: "01"},
      {Valor:"02", Nombre: "02"},
      {Valor:"03", Nombre: "03"},
      {Valor:"04",Nombre:"04"},
      {Valor:"05",Nombre:"05"},
      {Valor:"06",Nombre:"06"},
      {Valor:"07",Nombre:"07"},
      {Valor:"08",Nombre:"08"},
      {Valor:"09",Nombre:"09"},
      {Valor:"10",Nombre:"10"},
      {Valor:"11",Nombre:"11"},
      {Valor:"12",Nombre:"12"},
      {Valor:"13", Nombre: "13"},
      {Valor:"14", Nombre: "14"},
      {Valor:"15", Nombre: "15"},
      {Valor:"16",Nombre:"16"},
      {Valor:"17",Nombre:"17"},
      {Valor:"18",Nombre:"18"},
      {Valor:"19",Nombre:"19"},
      {Valor:"20",Nombre:"20"},
      {Valor:"21",Nombre:"21"},
      {Valor:"22",Nombre:"22"},
      {Valor:"23",Nombre:"23"},
      {Valor:"24",Nombre:"24"},
      {Valor:"25", Nombre:"25"},
      {Valor:"26",Nombre:"26"},
      {Valor:"27",Nombre:"27"},
      {Valor:"28",Nombre:"28"},
      {Valor:"29",Nombre:"29"},
      {Valor:"30",Nombre:"30"},
      {Valor:"31",Nombre:"31"},
      {Valor:"32",Nombre:"32"},
      {Valor:"33",Nombre:"33"},
      {Valor:"34",Nombre:"34"},
      {Valor:"35",Nombre:"35"},
      {Valor:"36",Nombre:"36"},
      {Valor:"37",Nombre:"37"},
      {Valor:"38",Nombre:"38"},
      {Valor:"39",Nombre:"39"},
      {Valor:"40",Nombre:"40"},
      {Valor:"41",Nombre:"41"},
      {Valor:"42",Nombre:"42"},
      {Valor:"43",Nombre:"43"},
      {Valor:"44",Nombre:"44"},
      {Valor:"45",Nombre:"45"},
      {Valor:"46",Nombre:"46"},
      {Valor:"47",Nombre:"47"},
      {Valor:"48",Nombre:"48"},
      {Valor:"49",Nombre:"49"},
      {Valor:"50",Nombre:"50"},
      {Valor:"51",Nombre:"51"},
      {Valor:"52",Nombre:"52"},
      {Valor:"53",Nombre:"53"},
      {Valor:"54",Nombre:"54"},
      {Valor:"55",Nombre:"55"},
      {Valor:"56",Nombre:"56"},
      {Valor:"57",Nombre:"57"},
      {Valor:"58",Nombre:"58"},
      {Valor:"59",Nombre:"59"},
      {Valor:"60",Nombre:"60"},
      
    ]),
    new Formulario().Campo('Descripción', 'descripcion', 'textarea', [Validators.required], 12),
    new Formulario().Campo('Id_franquicia', 'id_franquicia', null, [Validators.required], 12,[], this.Datos.id_franquicia),
  ];
  public CalendarioForm = new FormGroup({});

  constructor( private Http: HttpService, private ARoute: ActivatedRoute,
    public dialogRef: MatDialogRef<FranquiciatarioCalendarioComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any, private Ventana: VentanaService,
  private HTTP: HttpService, private Alertas: AlertasService)
  { }

  ngOnInit() {
    console.log(this.fecha_hoy);

    this.CargarTareas();
  }

  Guardar(op)
  {
    console.log(this.CalendarioForm.value);
    console.log("GUARDAR", op);

    let {value} = this.CalendarioForm;
    var franquiciatario =value;
    var Fecha = new Date(value.fecha_inicio);
    franquiciatario.fecha_inicio = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;
    
    this.HTTP.post("calendario/guardar", franquiciatario).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar1("Datos guardados", "Cerrar");
      }})}


      CargarTareas() {
        this.Procesando = true;
        this.Http.post("calendario/institucion", {Franquicia: +this.Datos['id_franquicia']}).subscribe((res: any) => {
          console.log(res);
          this.Tareas = res.datos.tareas;
          for(let i in this.Tareas) {
            this.Tareas[i].fecha_inicio = new Date(new Date(this.Tareas[i].fecha_alta).getTime() + (12 * 60 * 60 * 1000));
            this.Tareas[i].fecha_vencimiento = new Date(new Date(this.Tareas[i].fecha_vencimiento).getTime() + (12 * 60 * 60 * 1000));
            
          }
          this.Calendario = res.datos.calendario;
          this.Procesando = false;
        });
      }
      CambioTarea(tarea) {
        console.log(tarea);
        let Tarea: any = {};
        Tarea = Object.assign(Tarea, tarea);
        var Fecha = new Date(Tarea.fecha_alta);
        Tarea.fecha_alta = Fecha.getFullYear() + "-" + (Fecha.getMonth() + 1) + "-" + Fecha.getDate();
        Fecha = new Date(Tarea.fecha_vencimiento);
        Tarea.fecha_vencimiento = Fecha.getFullYear() + "-" + (Fecha.getMonth() + 1) + "-" + Fecha.getDate();
        Tarea.comentario = tarea.comentario;
        delete Tarea.documentos;
        delete Tarea.verMas;
        this.Http.post("calendario/guardar", Tarea).subscribe((res: any) => {
          if(res.proceso) {
            this.Alertas.MSB_Mostrar1("Tarea guardada", "Cerrar", {duration: 1000});
            this.CargarTareas();
          }
        })
      }


  CambioComentario(tarea) {
  }

  PortafolioAccion(arg, tarea) {
    if(arg === "Eliminar") {
      this.Http.post("calendario/eliminar", tarea.id).subscribe((res: any) => {
        console.log(res);
        if(res.proceso) {
          this.Alertas.MSB_Mostrar1("Tarea eliminada");
          this.CargarTareas();
        }
      })
    }
  }
/* 
  AgregarArchivo(i, tarea) {
    this.Ventana.Abrir(UploadComponent, {Ancho: '50%', Datos: {
      Ruta: 'calendario/archivos', 
      Carpeta: 'tareas', 
      Multiple: true,
      Nombres: false,
      Descripcion: false,
      Datos: {id_tarea: tarea.id}
    }}
    ).subscribe((res) => {
      console.log(res);
      if(res == 1) {
        this.CargarTareas();
      }
    })
  } */

}
