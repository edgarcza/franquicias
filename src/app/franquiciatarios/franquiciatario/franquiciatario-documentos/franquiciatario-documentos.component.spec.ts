import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatarioDocumentosComponent } from './franquiciatario-documentos.component';

describe('FranquiciatarioDocumentosComponent', () => {
  let component: FranquiciatarioDocumentosComponent;
  let fixture: ComponentFixture<FranquiciatarioDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatarioDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatarioDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
