import { Component, OnInit, Inject } from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UploadComponent } from 'src/app/modulos/upload/upload.component';
import { UploadDatos } from 'src/app/modulos/upload/upload';
import { VentanaService } from '../../../servicios/ventana.service'
import { HttpService} from '../../../servicios/http.service'
import {Tabla} from '../../../modulos/tabla/tabla';
import { AlertasService } from 'src/app/servicios/alertas.service';

@Component({
  selector: 'app-franquiciatario-documentos',
  templateUrl: './franquiciatario-documentos.component.html',
  styleUrls: ['./franquiciatario-documentos.component.css']
})

export class FranquiciatarioDocumentosComponent extends Tabla implements OnInit {
public Docs;
  Documents = [];

  constructor
  (
    public dialogRef: MatDialogRef<FranquiciatarioDocumentosComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any,
    private Ventana: VentanaService, public HTTP: HttpService, public Alertas: AlertasService
  ) 
  { 
   super(HTTP)
  }

  ngOnInit() 
  {
    this.HTTP.post("documentos", {Filtros:[{Campo:"id_lpf", Valor:this.Datos.id_franquicia, Condicion:"="}], Paginador: null}).subscribe((Docs:any)=>{
      console.log(Docs);
      this.Documents=Docs.datos;
    });
   }
   asd() {

    this.HTTP.post("documentos", { Filtros: [{ Campo: "id_lpf", Valor: this.Datos.id_franquicia, Condicion: "=" }], Paginador: null }).subscribe((Docs: any) => {
      console.log(Docs);
      this.Documents = Docs.datos;
    });
  }
  AgregarArchivo1() {
    console.log(this.Docs);
    let asd: UploadDatos = {
      Carpeta: 'documentos',
      Datos: {id:this.Datos.id_franquicia},
      Descripcion: true,
      Multiple: true,
      Nombres: true,
      Ruta: 'documentos/archivo',
    };

    console.log({});
    this.Ventana.Abrir(UploadComponent, {Ancho: '50%', Datos: asd}).subscribe((res) =>
    {
      console.log(res);
      if(res == 1) {
      }
    })     
  }
  
  GuardarDocumento(op) {
    delete this.Documents[op].id_principal;
    this.HTTP.post("documentos/guardar", this.Documents[op])
      .subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        }
      })
 
  }
  Opcion(i)
{
  {
    this.HTTP.post("documentos/eliminar", this.Documents[i]).subscribe((respuesta: any) => {
      if (respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Documentos eliminado", "Cerrar");
        this.Tabla();
      }
    })
  }
  this.asd();
}
}
