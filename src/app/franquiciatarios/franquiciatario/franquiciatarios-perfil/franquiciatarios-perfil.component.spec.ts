import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatariosPerfilComponent } from './franquiciatarios-perfil.component';

describe('FranquiciatariosPerfilComponent', () => {
  let component: FranquiciatariosPerfilComponent;
  let fixture: ComponentFixture<FranquiciatariosPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatariosPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatariosPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
