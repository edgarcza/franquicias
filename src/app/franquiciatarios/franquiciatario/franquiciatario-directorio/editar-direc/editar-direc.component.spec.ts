import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarDirecComponent } from './editar-direc.component';

describe('EditarDirecComponent', () => {
  let component: EditarDirecComponent;
  let fixture: ComponentFixture<EditarDirecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarDirecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarDirecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
