import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { LeadsNuevo } from '../../../../forms';
import { Formulario } from '../../../../modulos/formulario/formulario'
import { HttpService } from '../../../../servicios/http.service';
import { AlertasService } from '../../../../servicios/alertas.service';

@Component({
  selector: 'app-editar-direc',
  templateUrl: './editar-direc.component.html',
  styleUrls: ['./editar-direc.component.css']
})
export class EditarDirecComponent implements OnInit {
  Controles = [
    new Formulario().Campo('Nombre del contacto', 'contacto', 'text', [Validators.required], 12),
    new Formulario().Campo('Escuela', 'escuela', 'text', [Validators.required], 12),
    new Formulario().Campo('Teléfono', 'telefono', 'number', [Validators.required], 12),
    new Formulario().Campo('Correo', 'correo', 'text', [Validators.required, Validators.email], 12),
    new Formulario().Campo('Franquicia', 'id_franquicia', null, [Validators.required], 12, [], this.DDatos.id_franquicia ),

  ];

  public EditarDirecForm = new FormGroup({});

  constructor (
    public dialogRef: MatDialogRef<EditarDirecComponent>,
    @Inject(MAT_DIALOG_DATA) public DDatos: any,
    // public Formulario: DatosFormularioService
     private HTTP: HttpService, private Alertas: AlertasService)
    {
  }
  cerrar(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    
  }
  Cerrar(caso = 1) {
    this.dialogRef.close(caso);
  }

  Guardar(op)
  {
    
    let {value} = this.EditarDirecForm;
    var editar =value;
    console.log("GUARDAR", op);
    this.HTTP.post("directorio/guardar", editar).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        this.Cerrar(op);
      }})}
}
