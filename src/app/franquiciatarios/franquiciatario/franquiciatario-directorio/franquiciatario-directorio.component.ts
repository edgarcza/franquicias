import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../../modulos/formulario/formulario'
import {Tabla} from '../../../modulos/tabla/tabla'
import { HttpService } from '../../../servicios/http.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AlertasService } from '../../../servicios/alertas.service';
import { VentanaService } from '../../../servicios/ventana.service'
import { EditarDirecComponent } from './editar-direc/editar-direc.component';




@Component({
  selector: 'app-franquiciatario-directorio',
  templateUrl: './franquiciatario-directorio.component.html',
  styleUrls: ['./franquiciatario-directorio.component.css']
})
export class FranquiciatarioDirectorioComponent extends Tabla implements OnInit {
  


  DirectorioControles = [
    new Formulario().Campo('Nombre del contacto', 'contacto', 'text', [Validators.required], 12),
    new Formulario().Campo('Escuela', 'escuela', 'text', [Validators.required], 12),
    new Formulario().Campo('Teléfono', 'telefono', 'number', [Validators.required], 12),
    new Formulario().Campo('Correo', 'correo', 'text', [Validators.required, Validators.email], 12),
    new Formulario().Campo('Franquicia', 'id_franquicia', null, [Validators.required], 12, [], this.Datos.id_franquicia ),

  ];
  public DirectorioForm = new FormGroup({});

  constructor(
    private Ventana: VentanaService, 
    public dialogRef: MatDialogRef<FranquiciatarioDirectorioComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any,
    public HTTP: HttpService, private Alertas: AlertasService

    ) { 
    
    super(HTTP);
    
  }
  
  ngOnInit() {
    let prueba:string = this.Datos.id_franquicia;
  
    this.ConfigurarTabla(
      {
        Tabla:{
          Nombre:"directorio",Campos:[
            {
              Id:"id", Nombre: "ID", Ocultar: true, Campo: "id", Total: false
            }
            ,
            {
              Id:"contacto" , Nombre: "Contacto", Ocultar: false, Campo:"contacto", Total: false
            },
            {
              Id:"escuela" , Nombre: "Escuela", Ocultar: false, Campo:"escuela", Total: false
            },
            {
              Id:"telefono" , Nombre: "Teléfono", Ocultar: false, Campo:"telefono", Total: false
            },
            {
              Id:"correo" , Nombre: "Correo", Ocultar: false, Campo:"correo", Total: false
            }
          ],
          Acciones:  [
            {
              Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
            }
            ,
            {
              Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
            }]
          }
          },
          [{Campo:'id_franquicia',Valor:prueba,Condicion:'='}])
  }
  Accion(arg) {
    console.log(arg);
    if (arg.Accion == "Modificar") {
      arg.Datos.password = null;
    this.Ventana.Abrir(EditarDirecComponent, { Ancho: '90%',Datos: arg.Datos }).subscribe(
      respuesta => {
        console.log(respuesta);
          this.Tabla();
      });
    }
    else if (arg.Accion == "Eliminar") {
      this.HTTP.post("directorio/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Registro eliminado", "Cerrar");
          this.Tabla();
        }
      })
    }
}
  Cerrar(caso = 1) {
    this.dialogRef.close(caso);
  }

  Guardar(op)
  {
    console.log("GUARDAR", op);
    this.HTTP.post("directorio/guardar", this.DirectorioForm.value).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
      }})}

}