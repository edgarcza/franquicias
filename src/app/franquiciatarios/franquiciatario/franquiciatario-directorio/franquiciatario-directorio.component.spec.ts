import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciatarioDirectorioComponent } from './franquiciatario-directorio.component';

describe('FranquiciatarioDirectorioComponent', () => {
  let component: FranquiciatarioDirectorioComponent;
  let fixture: ComponentFixture<FranquiciatarioDirectorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciatarioDirectorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciatarioDirectorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
