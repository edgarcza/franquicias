import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadsPerfilComponent } from './leads-perfil.component';

describe('LeadsPerfilComponent', () => {
  let component: LeadsPerfilComponent;
  let fixture: ComponentFixture<LeadsPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadsPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadsPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
