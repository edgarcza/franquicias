import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../modulos/formulario/formulario'
import { VentanaService } from '../../servicios/ventana.service'
import { UploadComponent } from 'src/app/modulos/upload/upload.component';
import { AlertasService } from '../../servicios/alertas.service'
import { HttpService } from 'src/app/servicios/http.service';
@Component({
  selector: 'app-leads-perfil',
  templateUrl: './leads-perfil.component.html',
  styleUrls: ['./leads-perfil.component.css']
})
export class LeadsPerfilComponent implements OnInit {


  public Tareas = [];
  public Procesando = true;


  Controles = [
    new Formulario().Campo('ID', 'id', '', null, 6),
    new Formulario().Campo('ID_status', 'id_status', '', null, 6),

    new Formulario().Campo('Nombres', 'nombre', 'text', [Validators.required], 4),
    new Formulario().Campo('Apellidos', 'apellidos', 'text', [Validators.required], 4),
    new Formulario().Campo('ID', 'id', '', null, 4),
    new Formulario().Campo('WhatsApp', 'whatsapp', 'number', [Validators.required], 4),
    new Formulario().Campo('Correo electrónico', 'correo', 'text', [Validators.required, Validators.email], 4),
    new Formulario().Campo('Estado', 'estado', 'text', [Validators.required], 6
    ),
    new Formulario().Campo('Ciudad', 'ciudad', 'text', [Validators.required], 6
    ),
    new Formulario().Campo('C.P.', 'cp', 'number', [Validators.required], 4),
    new Formulario().Campo('Colonia', 'colonia', 'text', [Validators.required], 4),
    new Formulario().Campo('Calle y número', 'calle_numero', 'text', [Validators.required], 4),
    new Formulario().Campo('Fecha de nacimiento', 'fecha_nacimiento', 'date', [Validators.required], 4),

    new Formulario().Campo('Nacionalidad', 'nacionalidad', 'text', [Validators.required], 4),
    new Formulario().Campo('Ocupación', 'ocupacion', 'text', [Validators.required], 4),
    new Formulario().Campo('Fuente de referencia', 'fuente_referencia', 'select', [Validators.required], 4,
      [
        { Valor: "Email", Nombre: "E-mail" },
        { Valor: "Facebook", Nombre: "Facebook" },
        { Valor: "Google", Nombre: "Google" },
        { Valor: "Instagram", Nombre: "Instagram" },
        { Valor: "Publicidad", Nombre: "Publicidad" },
        { Valor: "Referido", Nombre: "Referido" },
        { Valor: "Website", Nombre: "Website EE" },
        { Valor: "Whatsapp", Nombre: "Whatsapp" }
      ]
    ), new Formulario().Campo('Posibilidad de venta', 'probabilidad_venta', 'select', [Validators.required], 4,
      [
        { Valor: "1", Nombre: "Alta" },
        { Valor: "2", Nombre: "Regular" },
        { Valor: "3", Nombre: "Baja" },
      ]
    ), new Formulario().Campo('Franquicia', 'id_franquicia', 'select', [Validators.required], 4,
      [

      ]
    ),
    new Formulario().Campo('Notas', 'notas', 'textarea', [Validators.required], 12),
  ];


  public LeadsNuevoForm = new FormGroup({});


  constructor(
    public dialogRef: MatDialogRef<LeadsPerfilComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any, 
    private Ventana: VentanaService, 
    private Http: HttpService, private Alertas: AlertasService
  ) { }

  cerrar(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    this.Http.get("franquicias").subscribe((Franchaises: any) => {
      console.log(Franchaises);
      new Formulario().CampoSelect(this.Controles, "id_franquicia", Franchaises.datos, "id", "nombre");
    });
  }




  Guardar(op) {
    console.log(this.LeadsNuevoForm.value);
    console.log("GUARDAR", op);
    let { value } = this.LeadsNuevoForm;
    var franquiciatario = value;
    var Fecha = new Date(value.fecha_nacimiento);
    franquiciatario.fecha_nacimiento = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;

    if(op == 1) {
      Fecha = new Date();
      franquiciatario.fecha_p = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;
      franquiciatario.id_status = 2;
    }
    this.Http.post("lpf/guardar", franquiciatario).subscribe((respuesta: any) => {
      console.log(respuesta);
      if (respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
      }
    })
  }

  asd() {
    console.log(this.LeadsNuevoForm.value);
  }

  AgregarArchivo() {
    this.Ventana.Abrir(UploadComponent, { Ancho: '50%', Datos: { Ruta: 'lpf/archivo', Carpeta: 'lpf', Nombre: this.Datos.id } }).subscribe((res) => {
      console.log(res);
      if (res == 1) {
      }
    })
  }

}

