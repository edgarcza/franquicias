import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../modulos/formulario/formulario'
import { HttpService } from '../../servicios/http.service';
import { AlertasService } from '../../servicios/alertas.service';

@Component({
  selector: 'app-leads-nuevo',
  templateUrl: './leads-nuevo.component.html',
  styleUrls: ['./leads-nuevo.component.css']
})
export class LeadsNuevoComponent implements OnInit {


  DatosLead = {
    nombre: null,
  };



  Controles = [
    new Formulario().Campo('id', 'id', null, [], 12),
    new Formulario().Campo('Nombres', 'nombre', 'text', [Validators.required], 12),
    new Formulario().Campo('Apellidos', 'apellidos', 'text', [Validators.required], 12),
    new Formulario().Campo('WhatsApp', 'whatsapp', 'text', [Validators.required], 12),
    new Formulario().Campo('Correo electrónico', 'correo', 'text', [Validators.required, Validators.email], 12),
    new Formulario().Campo('Codigo Postal','cp','number',[Validators.required], 12),
    new Formulario().Campo('Estado', 'estado', 'text', [Validators.required], 12),
    new Formulario().Campo('Ciudad', 'ciudad', 'text', [Validators.required], 12),
    new Formulario().Campo('id_status', 'id_status', null, [], 12,[],1),
    new Formulario().Campo('Fuente de referencia', 'fuente_referencia', 'select', [Validators.required], 12, 
      [
        {Valor: "Email", Nombre: "E-mail"},
        {Valor: "Facebook", Nombre: "Facebook"},
        {Valor: "Google", Nombre: "Google"},
        {Valor:"Instagram",Nombre:"Instagram"},
        {Valor:"Publicidad",Nombre:"Publicidad"},
        {Valor:"Referido",Nombre:"Referido"},
        {Valor:"Website",Nombre:"Website EE"},
        {Valor:"Whatsapp",Nombre:"Whatsapp"}
      ]
    ),
    
    new Formulario().Campo('Notas', 'notas', 'textarea', [Validators.required], 12),
  ];

  public LeadsNuevoForm = new FormGroup({});

  constructor (
    public dialogRef: MatDialogRef<LeadsNuevoComponent>,
    @Inject(MAT_DIALOG_DATA) public DDatos: any,
     private HTTP: HttpService, private Alertas: AlertasService)
    {
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  ngOnInit() {


        
  }
  Cerrar(caso = 1) {
    this.dialogRef.close(caso);
  }

  Guardar(op)
  {
    console.log("GUARDAR", op);
    this.HTTP.post("lpf/guardar", this.LeadsNuevoForm.value)
    .subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        this.Cerrar(op);
      }})}
}
