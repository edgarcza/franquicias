import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadsNuevoComponent } from './leads-nuevo.component';

describe('LeadsNuevoComponent', () => {
  let component: LeadsNuevoComponent;
  let fixture: ComponentFixture<LeadsNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadsNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadsNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
