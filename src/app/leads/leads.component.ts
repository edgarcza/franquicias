import { Component, OnInit, Input } from '@angular/core';
import { Tabla } from '../modulos/tabla/tabla'
import { VentanaService } from '../servicios/ventana.service'
import { LeadsNuevoComponent } from './leads-nuevo/leads-nuevo.component';
import { LeadsPerfilComponent } from './leads-perfil/leads-perfil.component';
import { HttpService } from '../servicios/http.service';
import { AlertasService } from '../servicios/alertas.service';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.css'],

})
export class LeadsComponent extends Tabla implements OnInit {


  constructor(private Ventana: VentanaService,
    public HTTP: HttpService,
    private Alertas: AlertasService
  ) {
    super(HTTP);
  }

  ngOnInit() {
    this.ConfigurarTabla(
      {
        Tabla: {
          Nombre: "lpf", Campos: [
            {
              Id: "nombre", Nombre: "Nombres", Ocultar: false, Campo: "lpf.nombre", Total: false
            },
            {
              Id: "apellidos", Nombre: "Apellidos", Ocultar: false, Campo: "lpf.apellidos", Total: false
            },
            {
              Id: "whatsapp", Nombre: "Whatsapp", Ocultar: false, Campo: "lpf.whatsapp", Total: false
            },
            {
              Id: "correo", Nombre: "Correo", Ocultar: false, Campo: "lpf.correo", Total: false
            },
            {
              Id: "estado", Nombre: "Estado", Ocultar: false, Campo: "lpf.estado", Total: false
            },
            {
              Id: "ciudad", Nombre: "Ciudad", Ocultar: false, Campo: "lpf.ciudad", Total: false
            },
            {
              Id: "cp", Nombre: "Código Postal", Ocultar: false, Campo: "lpf.cp", Total: false
            },
            {
              Id: "fuente_referencia", Nombre: "Fuente Referencia", Ocultar: false, Campo: "lpf.fuente_referencia", Total: false
            }
          ],
          Acciones:
            [
              {
                Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
              }
              ,
              {
                Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
              }]
        }
      },
      [{ Campo: 'id_status', Valor: '1', Condicion: '=' }]
    )
  }
  Agregar() {

    this.Ventana.Abrir(LeadsNuevoComponent, { Ancho: '60%', Datos: null }).subscribe(
      respuesta => {
        console.log(respuesta);
        if (respuesta == 1) {
          this.Tabla();
        }
        else if (respuesta == 2) {
          this.Tabla();
          this.Agregar();
        }
      }
    );
  }

  Accion(arg) {
    console.log(arg);
    if (arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(LeadsPerfilComponent, { Ancho: '90%', Datos: arg.Datos }).subscribe(
        respuesta => {
          console.log(respuesta);
            this.Tabla();
        });
    }
    else if (arg.Accion == "Eliminar") {
      this.HTTP.post("lpf/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Registro eliminado", "Cerrar");
          this.Tabla();
        }
      })
    }
  }

}
