import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ExcelService } from './servicios/excel.service';
import { TablaComponent } from './modulos/tabla/tabla.component';
import { ColumnasComponent } from './modulos/tabla/columnas.component';
import { FormularioComponent } from './modulos/formulario/formulario.component';
import {AddComponent} from'./modulos/add/add.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import{ ConfirmarComponent} from '././ventanas/confirmar/confirmar.component';
import { CerrarVentanaComponent } from './ventanas/cerrar-ventana/cerrar-ventana.component';
import { LeadsComponent } from './leads/leads.component';
import { LeadsNuevoComponent } from './leads/leads-nuevo/leads-nuevo.component';
import { LeadsPerfilComponent } from './leads/leads-perfil/leads-perfil.component';
import { ProspectosComponent } from './prospectos/prospectos.component';
import { ProspectosPerfilComponent } from './prospectos/prospectos-perfil/prospectos-perfil.component';
import { FranquiciatariosComponent } from './franquiciatarios/franquiciatarios.component';
import { FranquiciatariosPerfilComponent } from './franquiciatarios/franquiciatario/franquiciatarios-perfil/franquiciatarios-perfil.component';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FranquiciatarioComponent } from './franquiciatarios/franquiciatario/franquiciatario.component';
import { FranquiciatarioDocumentosComponent } from './franquiciatarios/franquiciatario/franquiciatario-documentos/franquiciatario-documentos.component';
import { FranquiciatarioMultimediaComponent } from './franquiciatarios/franquiciatario/franquiciatario-multimedia/franquiciatario-multimedia.component';
import { FranquiciatarioDirectorioComponent } from './franquiciatarios/franquiciatario/franquiciatario-directorio/franquiciatario-directorio.component';
import { FranquiciatarioCalendarioComponent } from './franquiciatarios/franquiciatario/franquiciatario-calendario/franquiciatario-calendario.component';
import { VentanaComponent } from './modulos/ventana/ventana.component';
import { from } from 'rxjs';
import { UploadComponent } from './modulos/upload/upload.component';
import { FooterComponent } from './footer/footer.component';
import { ProspectosNuevoComponent } from './prospectos/prospectos-nuevo/prospectos-nuevo.component';
import { FranquiciatariosNuevoComponent } from './franquiciatarios/franquiciatarios-nuevo/franquiciatarios-nuevo.component';
import { FranComponent } from './fran/fran.component';
import { EventosComponent } from './franquiciatarios/franquiciatario/franquiciatario-calendario/eventos/eventos.component';
import { EditarDirecComponent } from './franquiciatarios/franquiciatario/franquiciatario-directorio/editar-direc/editar-direc.component';
import { RegistroComponent } from './login/registro/registro.component';
import { UsuariosComponent } from './inicio/usuarios/usuarios.component';
import { UsuarioDatosComponent } from './inicio/usuarios/usuario-datos/usuario-datos.component';
import { FranquiciasComponent } from './franquicias/franquicias.component';
import { FranquiciasDatosComponent } from './franquicias/franquicias-datos/franquicias-datos.component';
import { DatosPersonaComponent } from './datos-persona/datos-persona.component';
import { DatosFranquiciaComponent } from './datos-franquicia/datos-franquicia.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    LeadsComponent,
    TablaComponent,
    FormularioComponent,
    ColumnasComponent,
    CerrarVentanaComponent,
    ConfirmarComponent,
    LeadsNuevoComponent,
    FormularioComponent,
    LeadsPerfilComponent,
    ProspectosComponent,
    ProspectosPerfilComponent,
    FranquiciatariosComponent,
    FranquiciatariosPerfilComponent,
    ColumnasComponent,
    LoginComponent,
    TablaComponent,
    DashboardComponent,
    FranquiciatarioComponent,
    FranquiciatarioDocumentosComponent,
    AddComponent,
    FranquiciatarioMultimediaComponent,
    FranquiciatarioDirectorioComponent,
    FranquiciatarioCalendarioComponent,
    VentanaComponent,
    ColumnasComponent,
    FormularioComponent,
    TablaComponent,
    UploadComponent,
    FooterComponent,
    ProspectosNuevoComponent,
    FranquiciatariosNuevoComponent,
    FranComponent,
    EventosComponent,
    EditarDirecComponent,
    RegistroComponent,
    UsuariosComponent,
    UsuarioDatosComponent,
    FranquiciasComponent,
    FranquiciasDatosComponent,
    DatosPersonaComponent,
    DatosFranquiciaComponent,
  ],  
  entryComponents: [
    ColumnasComponent,
    LeadsNuevoComponent,
    ProspectosNuevoComponent,
    FranquiciatariosNuevoComponent,
    LeadsPerfilComponent,
    ProspectosPerfilComponent,
    FranquiciatariosPerfilComponent,
    FranquiciatarioComponent,
    UploadComponent, EditarDirecComponent, 
    UsuariosComponent, 
    UsuarioDatosComponent,
    FranquiciasDatosComponent, 
    DatosFranquiciaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    RouterModule.forRoot([
      {path: 'DatosFranquicia/:id', component: DatosFranquiciaComponent},
      {path: 'DatosPersona/:id', component: DatosPersonaComponent}

    ])
  ],
  providers: [
    ExcelService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
