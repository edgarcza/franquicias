import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosFranquiciaComponent } from './datos-franquicia.component';

describe('DatosFranquiciaComponent', () => {
  let component: DatosFranquiciaComponent;
  let fixture: ComponentFixture<DatosFranquiciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosFranquiciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosFranquiciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
