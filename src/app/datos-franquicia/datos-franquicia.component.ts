import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute} from '@angular/router'
import { from } from 'rxjs';
import { Formulario } from '../modulos/formulario/formulario'
import {  FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from '../servicios/alertas.service';

@Component({
  selector: 'app-datos-franquicia',
  templateUrl: './datos-franquicia.component.html',
  styleUrls: ['./datos-franquicia.component.css']
})
export class DatosFranquiciaComponent implements OnInit {
  public Franquicia;
  Controles = [
    new Formulario().Campo('id', 'id', null, [], 12),
    new Formulario().Campo('Nombre', 'nombre', 'text', null, 6), 
    new Formulario().Campo('Código Postal', 'cp', 'text', [Validators.required], 4),
    new Formulario().Campo('Estado', 'estado', 'text', null, 4),
    new Formulario().Campo('Ciudad', 'ciudad', 'text', [Validators.required], 4),
    new Formulario().Campo('Colonia', 'colonia', 'text', [Validators.required], 4),
    new Formulario().Campo('Calle y número', 'calle_numero', 'text', [Validators.required], 4),
  ];

  public DatosFranquiciaForm =new FormGroup({});


  constructor(private Alertas: AlertasService,
    private Http: HttpService,
    private _route: ActivatedRoute) {
console.log(this._route.snapshot.paramMap.get('id'));
   }

  ngOnInit() {
 
    let id = +this._route.snapshot.paramMap.get('id');
    this.Http.post("franquicias", {Filtros: [{Campo:'id', Valor: +this._route.snapshot.params['id'], Condicion: '='}], Paginador: null}).subscribe((res:any)=>{console.log(res);
    this.Franquicia = res.datos[0];})
  }

  Guardar(op)
  {
    console.log("GUARDAR", op);
    this.Http.post("franquicias/guardar", this.DatosFranquiciaForm.value).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
      }})}
}
