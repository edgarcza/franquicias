import { Component, OnInit, NgModule } from '@angular/core';
import { SesionService } from '../servicios/sesion.service'
import { HttpService } from '../servicios/http.service';
import { NgModel } from '@angular/forms';
import { LOCALE_ID } from '@angular/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
@NgModule
({
  providers: [
    { provide: LOCALE_ID, useValue: "es-MX" }, //replace "en-US" with your locale
    //otherProviders...
  ]
})
export class DashboardComponent implements OnInit {
  panelOpenState = false;
  public Procesando = true;
  public Tareas = [];
  public Calendario = [];
  
  public fecha_hoy = new Date().getTime();
  


  
  Dashboard = {
    Leads: {},
    Prospectos: {},
    Franquiciatarios: {}
  }

  constructor(
    public Sesion: SesionService,
    private Http: HttpService
  ) { }

  ngOnInit() {
    this.Http.post('lpf/dashboard', {}).subscribe((res: any) => {
      console.log(res.datos);
      this.Dashboard = res.datos;
    })
    this.CargarTareas();
  }

  CargarTareas() {
    this.Procesando = true;
    this.Http.post("calendario/instituciondb", {Franquicia: +this.Sesion.Usuario.id_franquicia}).subscribe((res: any) => {
      console.log(res);
      this.Tareas = res.datos.tareas;
      for(let i in this.Tareas) {
        this.Tareas[i].fecha_inicio = new Date(new Date(this.Tareas[i].fecha_alta).getTime() + (12 * 60 * 60 * 1000));
         this.Tareas[i].fecha_vencimiento = new Date(new Date(this.Tareas[i].fecha_vencimiento).getTime() + (12 * 60 * 60 * 1000));
        
      }
      this.Calendario = res.datos.calendario;
      this.Procesando = false;
    });
  }

}
