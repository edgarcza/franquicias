import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranquiciasDatosComponent } from './franquicias-datos.component';

describe('FranquiciasDatosComponent', () => {
  let component: FranquiciasDatosComponent;
  let fixture: ComponentFixture<FranquiciasDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranquiciasDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranquiciasDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
