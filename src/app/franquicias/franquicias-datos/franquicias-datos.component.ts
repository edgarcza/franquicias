import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { Formulario } from '../../modulos/formulario/formulario'
import { HttpService } from '../../servicios/http.service';
import { AlertasService } from '../../servicios/alertas.service';
import {} from '../franquicias.component'
import { from } from 'rxjs';
@Component({
  selector: 'app-franquicias-datos',
  templateUrl: './franquicias-datos.component.html',
  styleUrls: ['./franquicias-datos.component.css']
})
export class FranquiciasDatosComponent implements OnInit {
  Controles = [
    new Formulario().Campo('id', 'id', null, [], 12),
    new Formulario().Campo('Nombre', 'nombre', 'text', null, 6), 
    new Formulario().Campo('Código Postal', 'cp', 'text', [Validators.required], 4),
    new Formulario().Campo('Estado', 'estado', 'text', null, 4),
    new Formulario().Campo('Ciudad', 'ciudad', 'text', [Validators.required], 4),
    new Formulario().Campo('Colonia', 'colonia', 'text', [Validators.required], 4),
    new Formulario().Campo('Calle y número', 'calle_numero', 'text', [Validators.required], 4),
  ];

  public FranquiciasDatosForm =new FormGroup({});

  constructor( public dialogRef: MatDialogRef<FranquiciasDatosComponent>,
    @Inject(MAT_DIALOG_DATA) public DDatos: any,
     private HTTP: HttpService,
      private Alertas: AlertasService) { }

      cerrar(): void {
        this.dialogRef.close();
      }


  ngOnInit() {
  }
  Cerrar(caso = 1) {
    this.dialogRef.close(caso);
  }
  Guardar(op)
  {
    console.log("GUARDAR", op);
    this.HTTP.post("franquicias/guardar", this.FranquiciasDatosForm.value).subscribe((respuesta: any) => {
      console.log(respuesta);
      if(respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        this.Cerrar(op);
      }})}
}
