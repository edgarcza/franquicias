import { Component, OnInit, Input } from '@angular/core';
import { Tabla } from '../modulos/tabla/tabla'
import { VentanaService } from '../servicios/ventana.service'
import { HttpService } from '../servicios/http.service';
import { AlertasService } from '../servicios/alertas.service';
import { SesionService } from '../servicios/sesion.service'
import { Formulario } from '../modulos/formulario/formulario'
import { FormGroup, Validators } from '@angular/forms';
import { FranquiciasDatosComponent } from './franquicias-datos/franquicias-datos.component';

@Component({
  selector: 'app-franquicias',
  templateUrl: './franquicias.component.html',
  styleUrls: ['./franquicias.component.css']
})
export class FranquiciasComponent extends Tabla implements OnInit {

  Valores = this.Datos.Filas;
public FrnaquiciasForm = new FormGroup({});
  constructor(
    private Ventana: VentanaService, 
    public HTTP: HttpService, 
    private Alertas: AlertasService,
    public Sesion: SesionService
    ) { super(HTTP) }

  ngOnInit() 
  {
    this.ConfigurarTabla(
      {
        Tabla:
        {
          Nombre: "franquicias", Campos: [
            {
              Id: "id", Nombre: "id", Ocultar: true, Campo: "id", Total: false
              },
            {
            Id: "nombre", Nombre: "Nombre", Ocultar: false, Campo: "nombre", Total: false
            },
            {
              Id: "estado", Nombre: "Estado", Ocultar: false, Campo: "estado", Total: false
              },
              {
                Id: "ciudad", Nombre: "Ciudad", Ocultar: false, Campo: "ciudad", Total: false
                }
                ,
                {
                  Id: "colonia", Nombre: "Colonia", Ocultar: false, Campo: "colonia", Total: false
                },
                {
                  Id: "calle_numero", Nombre: "Calle y número", Ocultar: false, Campo: "calle_numero", Total: false
                }
          ], Acciones: [{
            Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
          }
          ,
          {
            Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
          }]
        }
      }
    )
  }
  Accion(arg) {
    console.log(arg);
    
    console.log("Modificar");
    if (arg.Accion == "Modificar") 
    {
      console.log(this.Valores);

       arg.Datos.password = null;
    this.Ventana.Abrir(FranquiciasDatosComponent, { Ancho: '90%',Datos: arg.Datos }).subscribe(
      respuesta => {
        console.log(respuesta);
        if (respuesta == 1)
          this.Tabla();
      }); 
    } 
    else if (arg.Accion == "Eliminar") 
    {
      console.log("Eliminar");/* 
      this.HTTP.post("lpf/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Institución eliminada", "Cerrar");
          this.Tabla();
        }})
       */}
  }

  Agregar() {    
    
    this.Ventana.Abrir(FranquiciasDatosComponent, {Ancho: '60%', Datos: null}).subscribe(
      respuesta => {
        console.log(respuesta);
        if(respuesta == 1) {
          this.Tabla();
        }
        else if(respuesta == 2) {
          this.Tabla();
          this.Agregar();
        }
      }
    );
  }

}
