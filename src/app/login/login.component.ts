import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../modulos/formulario/formulario'
import { HttpService } from '../servicios/http.service';
import { SesionService } from '../servicios/sesion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  LoginControles = [
    new Formulario().Campo('Usuario', 'usuario', 'text', [Validators.required], 12),
    new Formulario().Campo('Contraseña', 'password', 'password', [Validators.required], 12)
  ];
  public LoginForm = new FormGroup({});

  constructor(private HTTP: HttpService, private Sesion:SesionService, private Router: Router) { }

  ngOnInit() {
  }

  Enviar(datos) {
    console.log(this.LoginForm.value);
    this.HTTP.post("login", this.LoginForm.value).subscribe((respuesta: {usuario?}) => {
      console.log(respuesta);

      localStorage.setItem("ss", respuesta.usuario.remember_token);
      this.Sesion.Verificar();
      this.Router.navigate(['/Inicio/Dashboard']);
    });
  }

  http() {
    this.Sesion.Verificar();
  }}