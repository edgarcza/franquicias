import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranComponent } from './fran.component';

describe('FranComponent', () => {
  let component: FranComponent;
  let fixture: ComponentFixture<FranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
