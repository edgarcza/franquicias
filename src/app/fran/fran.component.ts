import { Component, OnInit, Input } from '@angular/core';
import {Tabla} from '../modulos/tabla/tabla'
import {VentanaService} from '../servicios/ventana.service'
import { HttpService } from '../servicios/http.service';
import { AlertasService } from '../servicios/alertas.service';
import {FranquiciatariosNuevoComponent} from '../franquiciatarios/franquiciatarios-nuevo/franquiciatarios-nuevo.component';


@Component({
  selector: 'app-fran',
  templateUrl: './fran.component.html',
  styleUrls: ['./fran.component.css']
})
export class FranComponent extends Tabla implements OnInit {

  @Input() sum = 0;
  inputValue = 0;

  constructor(private Ventana: VentanaService, HTTP: HttpService, private Alertas: AlertasService) {
    super(HTTP);
   }

  ngOnInit() {
    this.ConfigurarTabla(
      {
        Tabla:{
          Nombre:"franquicia",Campos:[
            {
              Id:"nombre" , Nombre: "Nombre", Ocultar: false, Campo:"nombre", Total: false
            },
            {
              Id:"estado" , Nombre: "Estado", Ocultar: false, Campo:"estado", Total: false
            },
            {
              Id:"ciudad" , Nombre: "Ciudad", Ocultar: false, Campo:"ciudad", Total: false
            },
            {
              Id:"cp" , Nombre: "Código Postal", Ocultar: false, Campo:"cp", Total: false
            },
            {
              Id: "colonia", Nombre: "Colonia", Ocultar: false, Campo: "colonia", Total: false
            }
            ,
            {
              Id: "calle_numero", Nombre: "Calle y numero", Ocultar: false, Campo: "calle_numero", Total: false
            }
          ],
          Acciones:
          [
            {
              Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
            }
            ,
            {
              Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
            }]}},
          [{Campo:'id',Valor:'',Condicion:'='}])
  }

  Accion(arg) {
    /* console.log(arg);
    if (arg.Accion == "Modificar") {
      arg.Datos.password = null;
    this.Ventana.Abrir(FranquiciatarioComponent, { Ancho: '90%',Datos: arg.Datos }).subscribe(
      respuesta => {
        console.log(respuesta);
        //if (respuesta == 1)
          this.Tabla();
      });
    }
    else if (arg.Accion == "Eliminar") {
      this.HTTP.post("lpf/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Registro eliminado", "Cerrar");
          this.Tabla();
        }
      })
    } */
}
}
