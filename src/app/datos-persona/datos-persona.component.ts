import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { from } from 'rxjs';
import { Formulario } from '../modulos/formulario/formulario'
import { FormGroup, Validators, FormArray } from '@angular/forms';
import { HttpService } from 'src/app/servicios/http.service';
import { AlertasService } from '../servicios/alertas.service';
import { VentanaService } from '../servicios/ventana.service'
import { UploadComponent } from 'src/app/modulos/upload/upload.component';


@Component({
  selector: 'app-datos-persona',
  templateUrl: './datos-persona.component.html',
  styleUrls: ['./datos-persona.component.css']
})
export class DatosPersonaComponent implements OnInit {
  public Persona;

  Controles = [
    new Formulario().Campo('ID', 'id', '', null, 6),
    new Formulario().Campo('ID_status', 'id_status', '', null, 6),

    new Formulario().Campo('Nombres', 'nombre', 'text', [Validators.required], 4),
    new Formulario().Campo('Apellidos', 'apellidos', 'text', [Validators.required], 4),
    new Formulario().Campo('ID', 'id', '', null, 4),
    new Formulario().Campo('WhatsApp', 'whatsapp', 'number', [Validators.required], 4),
    new Formulario().Campo('Correo electrónico', 'correo', 'text', [Validators.required, Validators.email], 4),
    new Formulario().Campo('ID', 'id', '', null, 4),
    new Formulario().Campo('Estado', 'estado', 'text', [Validators.required], 4
    ),
    new Formulario().Campo('Ciudad', 'ciudad', 'text', [Validators.required], 4),
    new Formulario().Campo('ID', 'id', '', null, 4),
    new Formulario().Campo('C.P.', 'cp', 'number', [Validators.required], 4),
    new Formulario().Campo('Colonia', 'colonia', 'text', [Validators.required], 4),
    new Formulario().Campo('Calle y número', 'calle_numero', 'text', [Validators.required], 4),
    new Formulario().Campo('Fecha de nacimiento', 'fecha_nacimiento', 'date', [Validators.required], 4),

    new Formulario().Campo('Nacionalidad', 'nacionalidad', 'text', [Validators.required], 4),
    new Formulario().Campo('Ocupación', 'ocupacion', 'text', [Validators.required], 4),
    new Formulario().Campo('Fuente de referencia', 'fuente_referencia', 'select', [Validators.required], 4,
      [
        { Valor: "Email", Nombre: "E-mail" },
        { Valor: "Facebook", Nombre: "Facebook" },
        { Valor: "Google", Nombre: "Google" },
        { Valor: "Instagram", Nombre: "Instagram" },
        { Valor: "Publicidad", Nombre: "Publicidad" },
        { Valor: "Referido", Nombre: "Referido" },
        { Valor: "Website", Nombre: "Website EE" },
        { Valor: "Whatsapp", Nombre: "Whatsapp" }
      ]
    ), new Formulario().Campo('Posibilidad de venta', 'probabilidad_venta', 'select', [Validators.required], 4,
      [
        { Valor: "1", Nombre: "Alta" },
        { Valor: "2", Nombre: "Regular" },
        { Valor: "3", Nombre: "Baja" },
      ]
    ), new Formulario().Campo('Franquicia', 'id_franquicia', 'select', [Validators.required], 4,
      [

      ]
    ),
    new Formulario().Campo('Notas', 'notas', 'textarea', [Validators.required], 12),
  ];

  public DatosPersonaForm = new FormGroup({});

  constructor(private Http: HttpService, private _route: ActivatedRoute, private Alertas: AlertasService, private Ventana: VentanaService, ) {

  }

  ngOnInit() {

    this.Http.get("franquicias").subscribe((Franchaises: any) => {
      console.log(Franchaises);
      new Formulario().CampoSelect(this.Controles, "id_franquicia", Franchaises.datos, "id", "nombre");
    });


    let id = +this._route.snapshot.paramMap.get('id');
    this.Http.post("lpf", { Filtros: [{ Campo: 'id', Valor: +this._route.snapshot.params['id'], Condicion: '=' }], Paginador: null }).subscribe((res: any) => {
      console.log(res);
      this.Persona = res.datos[0];
    })


  }
  Guardar(op) {
    console.log(this.DatosPersonaForm.value);
    console.log("GUARDAR", op);
    let { value } = this.DatosPersonaForm;
    var franquiciatario = value;
    var Fecha = new Date(value.fecha_nacimiento);
    franquiciatario.fecha_nacimiento = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;

    if (op == 2) {
      Fecha = new Date();
      franquiciatario.fecha_p = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;
      franquiciatario.id_status = 2;
    }
    this.Http.post("lpf/guardar", franquiciatario).subscribe((respuesta: any) => {
      console.log(respuesta);
      if (respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
      }
    })
  }

  AgregarArchivo() {
    this.Ventana.Abrir(UploadComponent, { Ancho: '50%', Datos: { Ruta: 'lpf/archivo', Carpeta: 'lpf', Nombre: this.Persona.id } }).subscribe((res) => {
      console.log(res);
      if (res == 1) {
      }
    })
  }

}
