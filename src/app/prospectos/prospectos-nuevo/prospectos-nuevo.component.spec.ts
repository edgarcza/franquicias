import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectosNuevoComponent } from './prospectos-nuevo.component';

describe('ProspectosNuevoComponent', () => {
  let component: ProspectosNuevoComponent;
  let fixture: ComponentFixture<ProspectosNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectosNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectosNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
