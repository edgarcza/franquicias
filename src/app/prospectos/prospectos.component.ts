import { Component, OnInit } from '@angular/core';
import { Tabla } from '../modulos/tabla/tabla'
import { VentanaService } from '../servicios/ventana.service'
import { ProspectosPerfilComponent } from './prospectos-perfil/prospectos-perfil.component';
import { HttpService } from '../servicios/http.service';
import { AlertasService } from '../servicios/alertas.service';
import { ProspectosNuevoComponent } from './prospectos-nuevo/prospectos-nuevo.component';
// import { LeadsPerfilComponent } from './leads-perfil/leads-perfil.component';

@Component({
  selector: 'app-prospectos',
  templateUrl: './prospectos.component.html',
  styleUrls: ['./prospectos.component.css']
})
export class ProspectosComponent extends Tabla implements OnInit {

  constructor
    (
      private Ventana: VentanaService,
      HTTP: HttpService,
      private Alertas: AlertasService) {
    super(HTTP);
  }

  // public Datos = [];
  ngOnInit() {
    this.ConfigurarTabla(
      {
        Tabla: {
          Nombre: "lpf", Campos: [
            /* {
              Id:"foto" , Nombre: "Foto", Ocultar: false, Campo:"foto", Total: false
            }, */
            {
              Id: "nombre", Nombre: "Nombre", Ocultar: false, Campo: "nombre", Total: false
            },
            {
              Id: "apellidos", Nombre: "Apellidos", Ocultar: false, Campo: "apellidos", Total: false
            },
            {
              Id: "whatsapp", Nombre: "Whatsapp", Ocultar: false, Campo: "whatsapp", Total: false
            },
            {
              Id: "correo", Nombre: "Correo", Ocultar: false, Campo: "correo", Total: false
            },
            {
              Id: "estado", Nombre: "Estado", Ocultar: false, Campo: "estado", Total: false
            },
            {
              Id: "ciudad", Nombre: "Ciudad", Ocultar: false, Campo: "ciudad", Total: false
            },
            {
              Id: "cp", Nombre: "Código Postal", Ocultar: false, Campo: "cp", Total: false
            },
            {
              Id: "fuente_referencia", Nombre: "Fuente Referencia", Ocultar: false, Campo: "lpf.fuente_referencia", Total: false
            }
          ],
          Acciones:
            [
              {
                Nombre: 'Modificar', Tipo: 'interna', Accion: 'Modificar'
              },
              {
                Nombre: 'Eliminar', Tipo: 'interna', Accion: 'Eliminar'
              }
            ]
        }
      }
      ,
      [{ Campo: 'id_status', Valor: '2', Condicion: '=' }]
    )
  }

  Agregar() {
    this.Ventana.Abrir(ProspectosNuevoComponent, { Ancho: '60%', Datos: null }).subscribe(
      respuesta => {
        console.log(respuesta);
        if (respuesta == 2) {
          this.Tabla();
        }
        else if (respuesta == 1) {
          this.Tabla();
          this.Agregar();
        }
      }
    );
  }

  Accion(arg) {
    console.log(arg);
    if (arg.Accion == "Modificar") {
      arg.Datos.password = null;
      this.Ventana.Abrir(ProspectosPerfilComponent, { Ancho: '90%', Datos: arg.Datos }).subscribe(
        respuesta => {
          console.log(respuesta);
          // if (respuesta == 1)
          this.Tabla();
        });
    }
    else if (arg.Accion == "Eliminar") {
      this.HTTP.post("lpf/eliminar", arg.Datos.id).subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Institución eliminada", "Cerrar");
          this.Tabla();
        }
      })
    }
  }
}
