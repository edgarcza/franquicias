import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectosPerfilComponent } from './prospectos-perfil.component';

describe('ProspectosPerfilComponent', () => {
  let component: ProspectosPerfilComponent;
  let fixture: ComponentFixture<ProspectosPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectosPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectosPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
