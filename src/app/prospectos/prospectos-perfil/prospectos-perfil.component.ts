import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators } from '@angular/forms';
import { Formulario } from '../../modulos/formulario/formulario'
import { HttpService } from '../../servicios/http.service'
import { VentanaService } from '../../servicios/ventana.service'
import { UploadComponent } from 'src/app/modulos/upload/upload.component';
import { AlertasService } from 'src/app/servicios/alertas.service';
import { Tabla } from '../../modulos/tabla/tabla';
import { UploadDatos } from 'src/app/modulos/upload/upload';

@Component({
  selector: 'app-prospectos-perfil',
  templateUrl: './prospectos-perfil.component.html',
  styleUrls: ['./prospectos-perfil.component.css']
})



export class ProspectosPerfilComponent extends Tabla implements OnInit {

  Documents = [];

  NombreArchivo = document.getElementById("#NombreArchivo");
  NombreArchivoString = String(this.NombreArchivo);
  Tab = 0;

  Controles = [
    new Formulario().Campo('ID', 'id', '', null, 6),
    new Formulario().Campo('ID_status', 'id_status', '', null, 6),

    new Formulario().Campo('Nombres', 'nombre', 'text', [Validators.required], 4),
    new Formulario().Campo('Apellidos', 'apellidos', 'text', [Validators.required], 4),
    new Formulario().Campo('ID', 'id', '', null, 4),
    new Formulario().Campo('WhatsApp', 'whatsapp', 'number', [Validators.required], 4),
    new Formulario().Campo('Correo electrónico', 'correo', 'text', [Validators.required, Validators.email], 4),
    new Formulario().Campo('Estado', 'estado', 'text', [Validators.required], 6
    ),
    new Formulario().Campo('Ciudad', 'ciudad', 'text', [Validators.required], 6
    ),
    new Formulario().Campo('C.P.', 'cp', 'number', [Validators.required], 4),
    new Formulario().Campo('Colonia', 'colonia', 'text', [Validators.required], 4),
    new Formulario().Campo('Calle y número', 'calle_numero', 'text', [Validators.required], 4),
    new Formulario().Campo('Fecha de nacimiento', 'fecha_nacimiento', 'date', [Validators.required], 4),

    new Formulario().Campo('Nacionalidad', 'nacionalidad', 'text', [Validators.required], 4),
    new Formulario().Campo('Ocupación', 'ocupacion', 'text', [Validators.required], 4),
    new Formulario().Campo('Fuente de referencia', 'fuente_referencia', 'select', [Validators.required], 4,
      [
        { Valor: "Email", Nombre: "E-mail" },
        { Valor: "Facebook", Nombre: "Facebook" },
        { Valor: "Google", Nombre: "Google" },
        { Valor: "Instagram", Nombre: "Instagram" },
        { Valor: "Publicidad", Nombre: "Publicidad" },
        { Valor: "Referido", Nombre: "Referido" },
        { Valor: "Website", Nombre: "Website EE" },
        { Valor: "Whatsapp", Nombre: "Whatsapp" }
      ]
    ), new Formulario().Campo('Posibilidad de venta', 'probabilidad_venta', 'select', [Validators.required], 4,
      [
        { Valor: "1", Nombre: "Alta" },
        { Valor: "2", Nombre: "Regular" },
        { Valor: "3", Nombre: "Baja" },
      ]
    ), new Formulario().Campo('Franquicia', 'id_franquicia', 'select', [Validators.required], 4,
      []
    ),
    new Formulario().Campo('Notas', 'notas', 'textarea', [Validators.required], 12),
  ];


  /* Controles1 = [
    new Formulario().Campo('ID', 'id', '', null, 6),

    new Formulario().Campo('Estatus', 'estatus', 'select', [Validators.required], 10,
      [
        { Valor: "Activo", Nombre: "Activo" },
        { Valor: "Sin uso", Nombre: "Sin uso" },
        { Valor: "Pendiente", Nombre: "Pendiente" },
      ]
    )
  ]; */
  public ProspectosPerfilForm = new FormGroup({});
  /*public ProspectosDocForm = new FormGroup({});  */

  constructor(
    public dialogRef: MatDialogRef<ProspectosPerfilComponent>,
    @Inject(MAT_DIALOG_DATA) public Datos: any,
    private Ventana: VentanaService, public HTTP: HttpService,
    private Alertas: AlertasService
  ) {
    super(HTTP);
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.HTTP.get("franquicias").subscribe((Franchaises: any) => {
      console.log(Franchaises);
      new Formulario().CampoSelect(this.Controles, "id_franquicia", Franchaises.datos, "id", "nombre");
    });

    this.asd();
  }

  asd() {
    console.log(this.ProspectosPerfilForm.value);

    this.HTTP.post("documentos", { Filtros: [{ Campo: "id_lpf", Valor: this.Datos.id_franquicia, Condicion: "=" }], Paginador: null }).subscribe((Docs: any) => {
      console.log(Docs);
      this.Documents = Docs.datos;
    });
  }

  AgregarArchivo() {
    let asd: UploadDatos = {
      Carpeta: 'lpf',
      Datos: { id: this.Datos.id },
      Descripcion: false,
      Multiple: false,
      Nombres: false,
      Ruta: 'lpf/archivo',
    };

    this.Ventana.Abrir(UploadComponent, { Ancho: '50%', Datos: asd }).subscribe((res) => {
      console.log(res);
    })
  }

  AgregarArchivo1() {
    let asd: UploadDatos = {
      Carpeta: 'documentos',
      Datos: { id: this.Datos.id_franquicia },
      Descripcion: true,
      Multiple: true,
      Nombres: true,
      Ruta: 'documentos/archivo',
    };

    console.log({});
    this.Ventana.Abrir(UploadComponent, { Ancho: '50%', Datos: asd }).subscribe((res) => {
      console.log(res);
      this.asd();
      if (res == 1) {
      }
    })

  }

  GuardarDocumento(op) {
    delete this.Documents[op].id_principal;
    this.HTTP.post("documentos/guardar", this.Documents[op])
      .subscribe((respuesta: any) => {
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        }
      })
 
  }

Opcion(i)
{
  {
    this.HTTP.post("documentos/eliminar", this.Documents[i]).subscribe((respuesta: any) => {
      if (respuesta.proceso) {
        this.Alertas.MSB_Mostrar("Documentos eliminado", "Cerrar");
        this.Tabla();
      }
    })
  }
  this.asd();
}

  Guardar(op) {
    if (op == 2) {
      console.log(this.ProspectosPerfilForm.value);
      console.log("GUARDAR", op);

      let { value } = this.ProspectosPerfilForm;
      var franquiciatario = value;
      var Fecha = new Date(value.fecha_nacimiento);
      franquiciatario.fecha_nacimiento = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;

      this.HTTP.post("lpf/guardar", franquiciatario).subscribe((respuesta: any) => {
        console.log(respuesta);
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        }
      })
    }

    if (op == 1) {

      console.log(this.ProspectosPerfilForm.value);
      console.log("GUARDAR", op);

      let { value } = this.ProspectosPerfilForm;
      var franquiciatario = value;
      var Fecha = new Date(value.fecha_nacimiento);
      franquiciatario.fecha_nacimiento = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;
      franquiciatario.id_status = 3;

      Fecha = new Date();
      franquiciatario.fecha_f = `${Fecha.getFullYear()}-${Fecha.getMonth() + 1}-${Fecha.getDate()}`;
      franquiciatario.id_status = 2;

      this.HTTP.post("lpf/guardar", franquiciatario).subscribe((respuesta: any) => {
        console.log(respuesta);
        if (respuesta.proceso) {
          this.Alertas.MSB_Mostrar("Datos guardados", "Cerrar");
        }
      })

    }
  }
  trackByFunction(index: number, employee: any): string {
    return employee.code;
  }
}

