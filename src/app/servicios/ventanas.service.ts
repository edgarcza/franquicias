import { Component, OnInit, Inject, Injectable } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

// import { ColumnasComponent } from '../tablas/columnas/columnas.component';
// import { ConfirmarComponent } from '../ventanas/confirmar/confirmar.component'
// import { VentanaComponent } from '../ventanas/ventana/ventana.component'

@Injectable({
  providedIn: 'root'
})
export class VentanasService {

  constructor(
    public dialog: MatDialog,
    ) { }

  dialogRef;

  
  // Columnas(datos){
  //   console.log(datos);
  //   this.dialogRef = this.dialog.open(ColumnasComponent, {
  //     width: '30%',
  //     data: datos
  //   });

  //   this.dialogRef.afterClosed().subscribe(result => {
  //     console.log('[Ventana] Columnas cerrado');
  //   });
  // }

  // Confirmar(datos, alCerrarSI = function(){}, alCerrarNO = function(){}){
  //   // console.log(datos);
  //   this.dialogRef = this.dialog.open(ConfirmarComponent, {
  //     width: '30%',
  //     data: datos
  //   });

  //   this.dialogRef.afterClosed().subscribe(result => {
  //     console.log('[Ventana] Confirmar cerrado');
  //     if(result == true)
  //       alCerrarSI();
  //     else
  //       alCerrarNO();
  //   });
  // }

  // Ventana(componente, width, datos = {Titulo: 'Titulo', Datos: {}}, height = '70%', alCerrar = function(){}, alCerrarSI = function(){}, alCerrarNO = function(){}){
  //   // console.log(datos);
  //   this.dialogRef = this.dialog.open(componente, {
  //     width: width,
  //     // height: height,
  //     data: datos
  //   });

  //   this.dialogRef.afterClosed().subscribe(result => {
  //     console.log('[Ventana] Cerrado');

  //     if(result)
  //     {
  //       if(result == true)
  //         alCerrarSI();
  //       else
  //         alCerrarNO();
  //     }

  //     alCerrar();

  //   });
  // }

}
