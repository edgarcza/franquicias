import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
// import * as XLSX from 'xlsx/dist/xlsx.min';

@Injectable()
export class ExcelService {

  constructor() { }

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_${new Date().getTime()}.xlsx`;
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    // const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    // const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
    // XLSX.writeFile(workbook, ExcelService.toExportFileName(excelFileName));

    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    // var ws = XLSX.utils.aoa_to_sheet([["a","b"],[1,2]]);
    // XLSX.utils.sheet

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, excelFileName);

    /* save to file */
    XLSX.writeFile(wb, excelFileName+'.xlsx');
  }
  
}
