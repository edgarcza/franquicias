import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { MatDialog, MatDialogConfig } from "@angular/material";
import { VentanaComponent } from "../modulos/ventana/ventana.component";

@Injectable({
  providedIn: 'root'
})
export class VentanaService {

  constructor(private Dialog: MatDialog) { }

  /**
   * Create a point.
   * @param Componente - COMPONENTE
   * @param Datos - {Ancho: ?, Datos: ?}
   */
  Abrir(Componente: any, Datos: any = {Ancho: '300px', Datos: null}): Observable<any> {
      const dialogConfig = new MatDialogConfig();

      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      let DRef = this.Dialog.open(Componente, {
        width: Datos.Ancho,
        data: Datos.Datos,
      });

      return DRef.afterClosed();
  }

}
