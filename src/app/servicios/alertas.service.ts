import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AlertasService {

  constructor(public MatSnackBar: MatSnackBar) { }

  MSB_Mostrar(mensaje: string, accion: string, opciones: MatSnackBarConfig = {duration: 10000, horizontalPosition: 'right', verticalPosition: 'top', direction: 'ltr'}) {
    this.MatSnackBar.open(mensaje, accion, opciones);
  }
  MSB_Mostrar1(mensaje: string, accion: string = "Cerrar", opciones: MatSnackBarConfig = {}) {
    let opcs: MatSnackBarConfig = {duration: 10000, horizontalPosition: 'right', verticalPosition: 'top', direction: 'ltr'};
    Object.assign(opcs, opciones);
    this.MatSnackBar.open(mensaje, accion, opcs);
  }

}
