import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-cerrar-ventana',
  templateUrl: './cerrar-ventana.component.html',
  styleUrls: ['./cerrar-ventana.component.css']
})
export class CerrarVentanaComponent implements OnInit {

  @Output() clickCerrar = new EventEmitter();

  cerrar() {
    this.clickCerrar.emit();
  }

  constructor() { }

  ngOnInit() {
  }

}
